from pathlib import Path
import os
import os.path
from os import path
from itertools import islice

from Utils import Utils, BColors
from SystemUtils import SystemUtils


class FileIO:
    @classmethod
    def exists(cls) -> bool:
        if Path('filename.txt').is_file():
            return True
        else:
            return False

    @classmethod
    def read_first_n_lines(cls, file_path, n_lines) -> str:
        first_lines = ""
        with open(file=file_path, mode='r', encoding="utf8") as infile:
            lines_iterator = islice(infile, n_lines)
            for ll in lines_iterator:
                first_lines += ll.strip() + "\n"
        # END of...with...
        return first_lines

    @classmethod
    def write_to_file(cls, f_path, file_name, text):
        cls.create_dir(f_path)
        file_name = SystemUtils.merge_paths(f_path, file_name)

        if SystemUtils.is_windows():
            file_name = SystemUtils.to_windows_path_str(file_name)
        else:
            file_name = SystemUtils.to_unix_path_str(file_name)

        if cls.path_exists(file_name):
            print(BColors.FAIL + "Overwriting [" + file_name + "]" + BColors.ENDC)

        with open(file_name, "w") as file1:
            file1.write(text)
            file1.close()

    @classmethod
    def append_to_file(cls, f_path, file_name, text):
        cls.create_dir(f_path)
        with open(f_path + file_name, "a+") as file1:
            file1.write(text)
            file1.close()

    @classmethod
    def set_working_dir(cls, my_path):
        cls.create_dir(my_path)
        os.chdir(my_path)

    @classmethod
    def create_dir(cls, my_path):
        if not path.exists(my_path):
            os.makedirs(my_path)

    @classmethod
    def delete_contents(cls, my_path):
        import os, shutil
        folder = my_path ##'/path/to/folder'
        for filename in os.listdir(folder):
            file_path = os.path.join(folder, filename)
            try:
                if os.path.isfile(file_path) or os.path.islink(file_path):
                    os.unlink(file_path)
                elif os.path.isdir(file_path):
                    shutil.rmtree(file_path)
            except Exception as e:
                print('Failed to delete %s. Reason: %s' % (file_path, e))
    ## END of delete_contents()

    @classmethod
    def is_directory(cls, my_path):
        return os.path.isdir(my_path)

    @classmethod
    def is_file(cls, my_path):
        return os.path.isfile(my_path)

    @classmethod
    def path_exists(cls, my_path) -> bool:
        return path.exists(my_path)

    @classmethod
    def get_current_directory(cls):
        return os.getcwd()

    @classmethod
    def is_absolute_path(cls, my_path) -> bool:
        return os.path.isabs(my_path)

    @classmethod
    def get_absolute_path(cls, my_path) -> bool:
        return os.path.abspath(my_path)

    @classmethod
    def is_file_contains(cls, my_path, n_lines, text) -> bool:
        temp_text = cls.read_first_n_lines(my_path, n_lines)
        return text in temp_text

    @classmethod
    def test(cls):
        print("Ans: " + str(FileIO.is_directory("file\\")))


if __name__ == '__main__':
    FileIO.test()