import string
import random
from typing import List
from lxml import etree, objectify


from io import StringIO
from html.parser import HTMLParser

class MLStripper(HTMLParser):
    def __init__(self):
        super().__init__()
        self.reset()
        self.strict = False
        self.convert_charrefs= True
        self.text = StringIO()
    def handle_data(self, d):
        self.text.write(d)
    def get_data(self):
        return self.text.getvalue()

def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()

class LxmlHelper:
    @classmethod
    def objectify_xml(cls, input_path_dir):
        file_dom = etree.parse(input_path_dir)  # parse xml and convert it into DOM

        etree.strip_tags(file_dom, 'bold', 'computeroutput', 'formula', 'para', 'ref')

        file_xml_bin = etree.tostring(file_dom, pretty_print=False, encoding="ascii")  # encode DOM into ASCII object
        file_xml_text = file_xml_bin.decode()  # convert binary ASCII object into ASCII text

        ### print(file_xml_text)

        objectified_xml = objectify.fromstring(file_xml_text)  # convert text into a Doxygen object
        return objectified_xml

    @classmethod
    def object_to_text(cls, xml_obj):
        txt = ""
        for t in xml_obj.itertext(with_tail=True):
            txt += t
        return txt


class FileExtension:
    RST = "rst"
    TXT = "txt"

class XMLTypes:
    NAMESPACE = "kind=\"namespace\""
    CLASS = "kind=\"class\""
    STRUCT = "kind=\"struct\""


class BColors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class Utils:
    @staticmethod
    def concat(*args):
        return "".join(args)

    @staticmethod
    def get_file_name_to_write_on(name: str) -> str:
        char_list = [' ', ':', '<', '>', '(', ')', '&', '~', '*', ',', '.']
        definition = Utils.replace_chars(name, char_list, "_")
        return definition

    @staticmethod
    def key_exists(dictionary, key) -> bool:
        if key in dictionary:
            return True
        else:
            return False

    @staticmethod
    def is_text_exists(main_text, text) -> bool:
        return text in main_text

    @staticmethod
    def get_random_string(size):
        valid_chars = string.ascii_letters + string.digits
        return ''.join(random.choice(valid_chars) for _ in range(size))

    @classmethod
    def remove_unwanted_text(cls, text_):
        return text_.replace("<para>", "").replace("</para>", "")

    @classmethod
    def replace_dollar(cls, text):
        bracket = (":math:`", "`")  # Index this string with values True/False, which are 0/1
        right = False  # Indicates to use right bracket (or not)

        while '$' in text:  # In each iteration, replace the left-most occurrence
            text = text.replace('$', bracket[right], 1)
            right = not right  # Switch between left and right brackets
        return text

    @classmethod
    def replace_chars(cls, old_text: str, old_chars_list: List[str], new_char:str) -> str:
        try:
            for old_ch in old_chars_list:
                old_text = old_text.replace(old_ch, new_char)
            return old_text
        except Exception as e:
            print(str(e))
        return ""

    @classmethod
    def test(cls):
        name = r'E core::algorithms::trees::KDTreeNode< E >::element'
        definition = Utils.replace_chars(name, [' ', ':', '<', '>', '(', ')', '&', '~', '*', ',', '.'], "_")
        print(definition)


if __name__ == '__main__':
    Utils.test()