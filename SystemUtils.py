import os
import platform
from typing import Tuple, List

from PathValidation import PathValidation


class SystemUtils:
    @staticmethod
    def to_windows_path_str(path_string) -> str:
        path_string = path_string.replace("/", "\\")
        return path_string

    @staticmethod
    def to_unix_path_str(path_string) -> str:
        path_string = path_string.replace("\\", "/")
        return path_string

    @staticmethod
    def is_windows() -> bool:
        system_type = platform.system()
        return system_type == "Windows"

    @staticmethod
    def is_unix() -> bool:
        return not SystemUtils.is_windows()

    @staticmethod
    def split_path(path_string: str) -> Tuple[str, str]:
        left_part, right_part = os.path.split(path_string)
        return left_part, right_part

    @staticmethod
    def is_file_str(path_string: str) -> bool:
        is_valid = PathValidation.is_pathname_valid(path_string)

        if SystemUtils.is_windows():
            path_string = SystemUtils.to_windows_path_str(path_string)
        else:
            path_string = SystemUtils.to_unix_path_str(path_string)

        if is_valid:
            end_of_path_string = os.path.basename(path_string)

            if end_of_path_string == "":
                return False

            str_list = end_of_path_string.split(".")

            if len(str_list) > 1:
                return True
            else:
                return False
            # END of inner if:
        # END of outer if:
        return False

    @staticmethod
    def is_dir_str(path_string: str) -> bool:
        is_valid = PathValidation.is_pathname_valid(path_string)

        if SystemUtils.is_windows():
            path_string = SystemUtils.to_windows_path_str(path_string)
        else:
            path_string = SystemUtils.to_unix_path_str(path_string)

        if is_valid:
            end_of_path_string = os.path.basename(path_string)

            if end_of_path_string == "":
                return True

            str_list = end_of_path_string.split(".")

            if len(str_list) <= 1:
                return True
            else:
                return False
        # END of outer if:
        return False

    @staticmethod
    def get_path_string_split(path_string: str):
        is_file = SystemUtils.is_file_str(path_string)

        if is_file:
            split_ch = None

            if SystemUtils.is_windows():
                split_ch = "\\"
                path_string = SystemUtils.to_windows_path_str(path_string)
            else:
                split_ch = "/"
                path_string = SystemUtils.to_unix_path_str(path_string)
            # END of if-else:

            return path_string.split(split_ch)
        else:
            return None

    @staticmethod
    def get_file_name(path_string: str):
        str_list = SystemUtils.get_path_string_split(path_string)

        if str_list is not None:
            return str_list[-1]
        else:
            return ""


    @staticmethod
    def merge_paths(*str_list: str) -> str:
        str_list = list(filter(None, str_list))
        split_ch = None
        if SystemUtils.is_windows():
            split_ch = "\\"
        else:
            split_ch = "/"

        joined_path = ""

        if len(str_list) > 1:
            joined_path = split_ch.join(str_list)
        else:
            for item in str_list:
                joined_path += item

        if SystemUtils.is_windows():
            joined_path = SystemUtils.to_windows_path_str(joined_path)
            return joined_path.replace("\\\\\\", "\\").replace("\\\\", "\\")
        else:
            joined_path = SystemUtils.to_unix_path_str(joined_path)
            return joined_path.replace("///", "/").replace("//", "/")
    # END of ... merge_paths()

    @staticmethod
    def merge_paths_list(str_list: List[str]) -> str:
        split_ch = None
        if SystemUtils.is_windows():
            split_ch = "\\"
        else:
            split_ch = "/"

        joined_path = split_ch.join(str_list)

        if SystemUtils.is_windows():
            joined_path = SystemUtils.to_windows_path_str(joined_path)
            return joined_path.replace("\\\\\\", "\\").replace("\\\\", "\\")
        else:
            joined_path = SystemUtils.to_unix_path_str(joined_path)
            return joined_path.replace("///", "/").replace("//", "/")
    # END of ... merge_paths_list()

    @staticmethod
    def get_dir(path_string: str):
        str_list = SystemUtils.get_path_string_split(path_string)

        if str_list is not None:
            str_list.pop()

            return SystemUtils.merge_paths_list(str_list)
        else:
            return ""


