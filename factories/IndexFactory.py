from definitions.CompoundTagIN import CompoundTagIN
from definitions.IndexIN import IndexIN
from definitions.MemberTagIN import MemberTagIN


class IndexFactory:
    @classmethod
    def load_index(cls, doxygenindex):
        index_object = IndexIN()

        for comp in doxygenindex.compound:
            compound_tag_object = CompoundTagIN()
            compound_tag_object.reference_id = comp.get("refid")
            compound_tag_object.kind = comp.get("kind")
            compound_tag_object.name = comp.name

            if hasattr(comp, "member"):
                for mem in comp.member:
                    member_object = MemberTagIN()
                    member_object.reference_id = mem.get("refid")
                    member_object.kind = mem.get("kind")
                    member_object.name = mem.name
                    compound_tag_object.add_member(member_object)
                # end of inner for loop

            index_object.add_compound(compound_tag_object)
        # end of outer for loop

        return index_object

if __name__ == "__main__":
    import lxml.etree as etree
    from lxml import objectify
    from factories.IndexFactory import IndexFactory

    dom = etree.parse("xmls/index.xml")  # parse xml and concert it into DOM
    xmlBin = etree.tostring(dom, pretty_print=False, encoding="ascii")  # encode DOM into ASCII object
    xmlText = xmlBin.decode()  # convert binary ASCII object into ASCII text
    doxygenindex = objectify.fromstring(xmlText)  # convert text into a Doxygen object

    index = IndexFactory.load_index(doxygenindex)
    index.printf()