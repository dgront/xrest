from FileIO import FileIO
from Utils import LxmlHelper, Utils
from definitions.BaseDescription import BaseDescription
from definitions.BoldTag import BoldTag
from definitions.BriefDescription import BriefDescription
from definitions.ChildNodeTagBCL import ChildNodeTagBCL
from definitions.ClassStructCompoundDefinition import ClassStructCompoundDefinition
from definitions.CollaborationGraphTagDCL import CollaborationGraphTagDCL
from definitions.CompoundRefTagBCL import CompoundRefTagBCL
from definitions.ComputerOutputTag import ComputerOutputTag
from definitions.DetailedDescription import DetailedDescription
from definitions.EnumMemberDefinitionST import EnumMemberDefinitionST
from definitions.FormulaTag import FormulaTag
from definitions.FunctionMemberDefinitionCL import FunctionMemberDefinitionCL
from definitions.InbodyDescription import InbodyDescription
from definitions.IncludesTagBCL import IncludesTagBCL
from definitions.InheritanceGraphTagBCL import InheritanceGraphTagBCL
from definitions.LinkTagBCL import LinkTagBCL
from definitions.ListOfAllmembers import ListOfAllmembers
from definitions.LocationDefinitionCL import LocationDefinitionCL
from definitions.MemberTagCL import MemberTagCL
from definitions.NamespaceCompoundDefinition import NamespaceCompoundDefinition
from definitions.NodeTagBCL import NodeTagBCL
from definitions.ParamTagCL import ParamTagCL
from definitions.ParameterDescriptionTagCL import ParameterDescriptionTagCL
from definitions.ParameterItemTagCL import ParameterItemTagCL
from definitions.ParameterListTagCL import ParameterListTagCL
from definitions.ParameterNameListTagCL import ParameterNameListTagCL
from definitions.ParameterNameTagCL import ParameterNameTagCL
from definitions.PlainText import PlainText
from definitions.RefParamTagCL import RefParamTagCL
from definitions.SectionDefinitionCL import SectionDefinitionCL
from definitions.SimpleSectTagCL import SimpleSectTagCL
from definitions.TypedefMemberDefinition import TypedefMemberDefinition
from definitions.ValueParamDefinitionCL import ValueParamDefinitionCL
from definitions.VariableMemberDefinitionCL import VariableMemberDefinitionCL
from abc import ABC, abstractmethod
from TagTypeEnum import TagTypeEnum

class AbstractCompoundDefinitionFactory(ABC):
    # <editor-fold desc="prepare location tag">
    def __prepare_location_tag(self, location):
        location_object = LocationDefinitionCL()
        location_object.file = location.get("file")
        location_object.line = location.get("line")
        location_object.column = location.get("column")
        location_object.body_file = location.get("bodyfile")
        location_object.body_start = location.get("bodystart")
        location_object.body_end = location.get("bodyend")
        return location_object

    # </editor-fold>

    # <editor-fold desc="prepare inheritance graph">
    def __prepare_inheritance_graph(self, inheritance_graph):
        inheritance_graph_object = InheritanceGraphTagBCL()
        for nod in inheritance_graph.node:
            node_object = NodeTagBCL()
            node_object.id = nod.get("id")
            node_object.label = nod.label

            if hasattr(nod, "link"):
                link_obj = LinkTagBCL()
                link_obj.reference_id = nod.link.get("refid")
                node_object.link = link_obj

            if hasattr(nod, "childnode"):
                child_node_obj = ChildNodeTagBCL()
                child_node_obj.reference_id = nod.childnode.get("refid")
                child_node_obj.relation = nod.childnode.get("relation")
                node_object.child_node = child_node_obj

            inheritance_graph_object.add_node(node_object)

        return inheritance_graph_object

    # </editor-fold>

    # <editor-fold desc="prepare collaboration graph">
    def __prepare_collaboration_graph(self, collaboration_graph):
        collaboration_graph_object = CollaborationGraphTagDCL()
        for nod in collaboration_graph.node:
            node_object = NodeTagBCL()
            node_object.id = nod.get("id")
            node_object.label = nod.label

            if hasattr(nod, "link"):
                link_obj = LinkTagBCL()
                link_obj.reference_id = nod.link.get("refid")
                node_object.link = link_obj
            # END---if hasattr(nod, "link")
            if hasattr(nod, "childnode"):
                child_node_obj = ChildNodeTagBCL()
                child_node_obj.reference_id = nod.childnode.get("refid")
                child_node_obj.relation = nod.childnode.get("relation")
                node_object.add_child_node(child_node_obj)
            # END---if hasattr(nod, "childnode")
            collaboration_graph_object.add_node(node_object)
        # END---for...collaboration_graph.node:
        return collaboration_graph_object

    # END---def __prepare_collaboration_graph()
    # </editor-fold>

    # <editor-fold desc="prepare includes tag">
    def __prepare_includes_tag(self, includes):
        includes_obj = IncludesTagBCL()
        includes_obj.reference_id = includes.get("refid")
        includes_obj.local = includes.get("local")
        includes_obj.name = includes
        return includes_obj

    # </editor-fold>

    # <editor-fold desc="prepare derived compound ref">
    def __prepare_derived_compound_ref(self, derivedcompoundref):
        derivedcompoundref_obj = CompoundRefTagBCL()
        derivedcompoundref_obj.reference_id = derivedcompoundref.get("refid")
        derivedcompoundref_obj.protection = derivedcompoundref.get("prot")
        derivedcompoundref_obj.virtual = derivedcompoundref.get("virt")
        derivedcompoundref_obj.name = derivedcompoundref.text
        return derivedcompoundref_obj
    # </editor-fold>

    # <editor-fold desc="prepare base compound_ref">
    def __prepare_base_compound_ref(self, basecompoundref):
        basecompoundref_obj = CompoundRefTagBCL()
        basecompoundref_obj.reference_id = basecompoundref.get("refid")
        basecompoundref_obj.protection = basecompoundref.get("prot")
        basecompoundref_obj.virtual = basecompoundref.get("virt")
        basecompoundref_obj.name = basecompoundref.text
        return basecompoundref_obj
    # </editor-fold>

    # <editor-fold desc="add params to a member definition object">
    def _add_param_s(self, member_definition_object, mem_def):
        for para in mem_def.param:
            param_obj = None
            if hasattr(para, "type"):
                if hasattr(para.type, "ref"):
                    # <memberdef>
                    #     <param>
                    #         <type>
                    #             <ref refid="abc12300xyz__refid" kindref="abc12300xyz__kindref">abc12300xyz -> ref</ref>
                    #         </type>
                    #         <declname>abc12300xyz__param -> declname</declname>
                    #     </param>
                    # </memberdef>
                    param_obj = RefParamTagCL()
                    param_obj.reference_id = para.type.ref.get("refid")
                    param_obj.kind_reference = para.type.ref.get("kindref")
                    param_obj.text = para.type.text
                    param_obj.tail = para.type.ref.tail
                    param_obj.type_name = para.type.ref.text
                    # if there is <declname>, add this info
                    if hasattr(para, "declname"):
                        param_obj.declaration_name = para.declname.text
                    if hasattr(para, "defval"):
                        param_obj.definition_value = para.defval.text
                elif hasattr(para, "declname"):
                    # <memberdef>
                    #     <param>
                    #         <type>abc12300xyz__param -> type</type>
                    #         <declname>abc12300xyz__param -> declname</declname>
                    #     </param>
                    # </memberdef>
                    param_obj = ValueParamDefinitionCL()
                    param_obj.type_name = para.type
                    param_obj.declaration_name = para.declname
                else:
                    # <memberdef>
                    #     <param>
                    #         <type>abc12300xyz__param -> type</type>
                    #     </param>
                    # </memberdef>
                    param_obj = ParamTagCL()
                    param_obj.type_name = para.type
                # end of inner if
            # end of outer if
            member_definition_object.add_parameter(param_obj)
    # </editor-fold>

    # <editor-fold desc="_add_enumvalue_s">
    def _add_enumvalue_s(self, member_definition_object, mem_def):
        for enumv in mem_def.enumvalue:
            enumvalue_obj = EnumMemberDefinitionST()
            enumvalue_obj.id = enumv.get("id")
            enumvalue_obj.protection = enumv.get("prot")
            enumvalue_obj.name = enumv.get("name")
            enumvalue_obj.brief_description = enumv.briefdescription
            enumvalue_obj.detailed_description = enumv.detaileddescription
            member_definition_object.add_enumvalue_def(enumvalue_obj)
    # </editor-fold>

    # <editor-fold desc="(prepare parameter item)">
    def _prepare_parameteritem__(self, lxml_parameteritem):
        parameter_item_obj = ParameterItemTagCL()

        if hasattr(lxml_parameteritem, "parameternamelist"):
            parameter_name_list_obj = ParameterNameListTagCL()
            lxml_parameternamelist = lxml_parameteritem.parameternamelist

            parameter_name_obj = ParameterNameTagCL()
            parameter_name_obj.text = lxml_parameternamelist.parametername.text
            parameter_name_list_obj.set_parameter_name(parameter_name_obj)
            parameter_item_obj.set_parameter_name_list(parameter_name_list_obj)
        # END of IF

        if hasattr(lxml_parameteritem, "parameterdescription"):
            parameter_description_obj = ParameterDescriptionTagCL()
            lxml_parameterdescription = lxml_parameteritem.parameterdescription

            if lxml_parameterdescription.text is not None:
                desc_str = lxml_parameterdescription.text
            else:
                desc_str = ""
            # END of ... if

            parameter_description_obj.text = desc_str.strip()
            parameter_item_obj.set_parameter_description(parameter_description_obj)
        # END of IF

        return parameter_item_obj
    # </editor-fold>

    def __prepare_description(self, lxml_description, description_obj: BaseDescription):
        if lxml_description.text is not None:
            temp_str = lxml_description.text
        else:
            temp_str = ""
        # END IF
        description_obj.text = temp_str.strip()

        if hasattr(lxml_description, "parameterlist"):
            lxml_parameterlist = lxml_description.parameterlist
            if hasattr(lxml_parameterlist, "parameteritem"):
                parameter_list_obj = ParameterListTagCL()
                for lxml_parameteritem in lxml_parameterlist.parameteritem:
                    param_item_obj = self._prepare_parameteritem__(lxml_parameteritem)
                    parameter_list_obj.add_item(param_item_obj)
                # END of FOR
                description_obj.set_parameter_list(parameter_list_obj)
            # END of inner IF
        # END of outer IF
    # END of function


    # <editor-fold desc="Description">
    def __prepare_brief_description(self, lxml_briefdescription):
        description_obj = BriefDescription()
        self.__prepare_description(lxml_briefdescription, description_obj)
        return description_obj

    def __prepare_detailed_description(self, lxml_detaileddescription):
        description_obj = DetailedDescription()
        self.__prepare_description(lxml_detaileddescription, description_obj)
        return description_obj

    def __prepare_inbody_description(self, lxml_inbodydescription):
        description_obj = InbodyDescription()
        self.__prepare_description(lxml_inbodydescription, description_obj)
        return description_obj
    # </editor-fold>

    def __add_formula_to_para(self, para_obj, formula):
        for form in formula:
            form_obj = FormulaTag()
            form_obj.id = form.id
            form_obj.text = form.text
            para_obj.add_string_item(form.tail)
            para_obj.add_formula_item(form_obj)

    # <editor-fold desc="(prepare location definition)">
    def __prepare_location(self, mem_def):
        location_object = LocationDefinitionCL()
        location_object.file = mem_def.location.get("file")
        location_object.line = mem_def.location.get("line")
        location_object.column = mem_def.location.get("column")
        location_object.body_file = mem_def.location.get("bodyfile")
        location_object.body_start = mem_def.location.get("bodystart")
        location_object.body_end = mem_def.location.get("bodyend")
        return location_object
    # </editor-fold>

    # <editor-fold desc="(prepare variable member)">
    def __prepare_variable_member(self, mem_def):
        member_definition_object = VariableMemberDefinitionCL()
        # add attributes of <memberdef> to member_definition_object
        member_definition_object.kind = mem_def.get("kind")
        member_definition_object.id = mem_def.get("id")
        member_definition_object.protection = mem_def.get("prot")
        member_definition_object.static = mem_def.get("static")
        member_definition_object.mutable = mem_def.get("mutable")
        # add child tags of <memberdef> to member_definition_object
        member_definition_object.data_type = mem_def.type
        member_definition_object.definition = mem_def.definition.text
        member_definition_object.args_string = mem_def.argsstring
        member_definition_object.name = mem_def.name
        return member_definition_object
    # </editor-fold>

    # <editor-fold desc="(prepare function member)">
    def __prepare_function_member(self, mem_def):
        member_definition_object = FunctionMemberDefinitionCL()
        member_definition_object.kind = mem_def.get("kind")
        member_definition_object.id = mem_def.get("id")
        member_definition_object.protection = mem_def.get("prot")
        member_definition_object.static = mem_def.get("static")
        member_definition_object.constant = mem_def.get("const")
        member_definition_object.explicit = mem_def.get("explicit")
        member_definition_object.inline = mem_def.get("inline")
        member_definition_object.virtual = mem_def.get("virt")

        if hasattr(mem_def, "type"):
            member_definition_object.data_type = mem_def.type.text

        if hasattr(mem_def, "definition"):
            member_definition_object.definition = mem_def.definition.text

        if hasattr(mem_def, "argsstring"):
            member_definition_object.args_string = mem_def.argsstring.text

        if hasattr(mem_def, "name"):
            member_definition_object.name = mem_def.name.text

        if hasattr(mem_def, "param"):
            self._add_param_s(member_definition_object, mem_def)

        return member_definition_object
    # </editor-fold>

    # <editor-fold desc="__prepare_typedef_member">
    def __prepare_typedef_member(self, mem_def):
        member_definition_object = TypedefMemberDefinition()
        # add attributes of <memberdef> to member_definition_object
        member_definition_object.kind = mem_def.get("kind")
        member_definition_object.id = mem_def.get("id")
        member_definition_object.protection = mem_def.get("prot")
        member_definition_object.static = mem_def.get("static")
        # add child tags of <memberdef> to member_definition_object
        member_definition_object.data_type = mem_def.type
        member_definition_object.definition = mem_def.definition
        member_definition_object.args_string = mem_def.argsstring
        member_definition_object.name = mem_def.name
        return member_definition_object
    # </editor-fold>

    # <editor-fold desc="__prepare_enum_member">
    def __prepare_enum_member(self, mem_def):
        member_definition_object = EnumMemberDefinitionST()
        # add attributes of <memberdef> to member_definition_object
        member_definition_object.kind = mem_def.get("kind")
        member_definition_object.id = mem_def.get("id")
        member_definition_object.protection = mem_def.get("prot")
        member_definition_object.static = mem_def.get("static")
        member_definition_object.name = mem_def.name

        if hasattr(mem_def, "enumvalue"):
            self._add_enumvalue_s(member_definition_object, mem_def)

        return member_definition_object
    # </editor-fold>

    # <editor-fold desc="__prepare_member_def">
    def __prepare_member_def(self, memdef):
        memberdef_obj = None
        kind = memdef.get("kind")
        if kind == "variable":
            memberdef_obj = self.__prepare_variable_member(memdef)
        elif kind == "function" or kind == "friend":
            memberdef_obj = self.__prepare_function_member(memdef)
        elif kind == "typedef":
            memberdef_obj = self.__prepare_typedef_member(memdef)
        elif kind == "enum":
            memberdef_obj = self.__prepare_enum_member(memdef)

        if hasattr(memdef, "briefdescription"):
            obj_brief = self.__prepare_brief_description(memdef.briefdescription)
            memberdef_obj.set_brief_description(obj_brief)

        if hasattr(memdef, "detaileddescription"):
            obj_detailed = self.__prepare_detailed_description(memdef.detaileddescription)
            memberdef_obj.set_detailed_description(obj_detailed)

        if hasattr(memdef, "inbodydescription"):
            obj_inbody = self.__prepare_inbody_description(memdef.detaileddescription)
            memberdef_obj.set_in_body_description(obj_inbody)

        if hasattr(memdef, "location"):
            obj_loc = self.__prepare_location(memdef)
            memberdef_obj.set_location(obj_loc)

        return memberdef_obj

    # </editor-fold>

    # <editor-fold desc="add memberdef to section definition object">
    def __prepare_section(self, section_def):
        section_obj = SectionDefinitionCL()
        section_obj.kind = section_def.get("kind")

        for memdef in section_def.memberdef:
            obj = self.__prepare_member_def(memdef)
            section_obj.add_member(obj)
        # end of FOR...section_def.memberdef

        return section_obj

    # </editor-fold>

    def load(self, doxygen):
        if not hasattr(doxygen, "compounddef"):
            return None

        kind = doxygen.compounddef.get("kind")
        compounddef_obj = None
        if kind == "class" or kind == "struct":
            compounddef_obj = ClassStructCompoundDefinition()
        if kind == "namespace":
            compounddef_obj = NamespaceCompoundDefinition()
            # end of if-elif-else

        # read attributes...
        compounddef_obj.id = doxygen.compounddef.get("id")
        compounddef_obj.kind = doxygen.compounddef.get("kind")
        compounddef_obj.language = doxygen.compounddef.get("language")
        compounddef_obj.protection = doxygen.compounddef.get("prot")

        if hasattr(doxygen.compounddef, "compoundname"):
            compounddef_obj.compound_name = doxygen.compounddef.compoundname.text

        if hasattr(doxygen.compounddef, "derivedcompoundref"):
            obj = self.__prepare_derived_compound_ref(doxygen.compounddef.derivedcompoundref)
            compounddef_obj.set_derived_compound_reference(obj)

        if hasattr(doxygen.compounddef, "basecompoundref"):
            obj = self.__prepare_base_compound_ref(doxygen.compounddef.basecompoundref)
            compounddef_obj.set_base_compound_reference(obj)

        if hasattr(doxygen.compounddef, "includes"):
            obj = self.__prepare_includes_tag(doxygen.compounddef.includes)
            compounddef_obj.set_includes(obj)

        # A <compounddef> tag can have multiple <sectiondef> tags.
        # Go though each <sectiondef> tag and convert them into objects
        if hasattr(doxygen.compounddef, "sectiondef"):
            for section_def in doxygen.compounddef.sectiondef:
                obj = self.__prepare_section(section_def)
                compounddef_obj.add_section_def(obj)

        if hasattr(doxygen.compounddef, "memberdef"):
            for memdef in doxygen.compounddef.memberdef:
                obj = self.__prepare_member_def(memdef)
                compounddef_obj.add_member_def(obj)
            # end of FOR...doxygen.compounddef.memberdef

        if hasattr(doxygen.compounddef, "briefdescription"):
            desc_obj = self.__prepare_brief_description(doxygen.compounddef.briefdescription)
            compounddef_obj.set_brief_description(desc_obj)

        if hasattr(doxygen.compounddef, "detaileddescription"):
            desc_obj = self.__prepare_detailed_description(doxygen.compounddef.detaileddescription)
            compounddef_obj.set_detailed_description(desc_obj)

        if hasattr(doxygen.compounddef, "inheritancegraph"):
            compounddef_obj.inheritance_graph = self.__prepare_inheritance_graph(
                doxygen.compounddef.inheritancegraph)

        if hasattr(doxygen.compounddef, "collaborationgraph"):
            compounddef_obj.collaboration_graph = self.__prepare_collaboration_graph(
                doxygen.compounddef.collaborationgraph)

        if hasattr(doxygen.compounddef, "listofallmembers"):
            listofallmembers_obj = ListOfAllmembers()
            # add <member> tags to listofallmembers_obj
            for member in doxygen.compounddef.listofallmembers.member:
                member_obj = MemberTagCL()
                member_obj.reference_id = member.get("refid")
                member_obj.protection = member.get("prot")
                member_obj.virtual = member.get("virt")
                member_obj.scope = member.scope
                member_obj.name = member.name
                listofallmembers_obj.add_member(member_obj)
            # END --- FOR...doxygen.compounddef.listofallmembers.member
            compounddef_obj.set_list_of_all_members(listofallmembers_obj)

        # add the <location> to compounddef_obj
        if hasattr(doxygen.compounddef, "location"):
            location_obj = self.__prepare_location_tag(doxygen.compounddef.location)
            compounddef_obj.set_location(location_obj)

        # end of outer for loop
        return compounddef_obj




if __name__ == "__main":
    pass
