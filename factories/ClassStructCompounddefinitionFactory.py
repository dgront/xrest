from typing import List

from Utils import LxmlHelper
from definitions.AbstractCompoundDefinition import AbstractCompoundDefinition
from definitions.ClassStructCompoundDefinition import ClassStructCompoundDefinition
from factories.AbstractCompoundDefinitionFactory import AbstractCompoundDefinitionFactory


class ClassStructCompoundDefinitionFactory(AbstractCompoundDefinitionFactory):
    def __init__(self):
        super().__init__()

    def load(self, input_path_dir: str) -> ClassStructCompoundDefinition:
        doxygen = LxmlHelper.objectify_xml(input_path_dir)
        return super().load(doxygen)