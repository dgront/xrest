import lxml.etree as etree
from lxml import objectify
from definitions.DirectoryCompoundDefinition import DirectoryCompoundDefinition
from definitions.InnerFileDefinitionDR import InnerFileDefinitionDR
from definitions.LocationDefinitionCL import LocationDefinitionCL


class DirectoryDefinitionFactory:
    @classmethod
    def load_dir(cls, doxygen):
        kind = doxygen.compounddef.get("kind")
        if kind == "dir":
            directory_definition_object = DirectoryCompoundDefinition()
            directory_definition_object.id = doxygen.compounddef.get("id")
            directory_definition_object.kind = doxygen.compounddef.get("kind")
            directory_definition_object.compound_name = doxygen.compounddef.compoundname

            # add inner file information
            if hasattr(doxygen.compounddef, "innerfile"):
                for inner in doxygen.compounddef.innerfile:
                    inner_file_object = InnerFileDefinitionDR()
                    inner_file_object.reference_id = inner.get("refid")
                    directory_definition_object.add_inner_file(inner_file_object)

            # add rest of the attributes
            directory_definition_object.brief_description = doxygen.compounddef.briefdescription
            directory_definition_object.detailed_description = doxygen.compounddef.detaileddescription

            # add location information
            location_object = LocationDefinitionCL()
            location_object.file = doxygen.compounddef.location.get("file")
            directory_definition_object.location = location_object

            return directory_definition_object

if __name__ == '__main__':
    dom = etree.parse("xmls/dir.xml")  # parse xml and concert it into DOM
    xmlBin = etree.tostring(dom, pretty_print=False, encoding="ascii")  # encode DOM into ASCII object
    xmlText = xmlBin.decode()  # convert binary ASCII object into ASCII text
    doxygen = objectify.fromstring(xmlText)  # convert text into a Doxygen object

    dir_def = DirectoryDefinitionFactory.load_dir(doxygen)
    print(dir_def)