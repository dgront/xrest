from Utils import LxmlHelper
from definitions.AbstractCompoundDefinition import AbstractCompoundDefinition
from definitions.InnerClassTagNS import InnerClassTagNS
from definitions.LocationDefinitionCL import LocationDefinitionCL
from definitions.NamespaceCompoundDefinition import NamespaceCompoundDefinition
from factories.AbstractCompoundDefinitionFactory import AbstractCompoundDefinitionFactory


class NamespaceCompoundDefinitionFactory(AbstractCompoundDefinitionFactory):
    def __init__(self):
        super().__init__()

    def load(self, input_path_dir : str) -> NamespaceCompoundDefinition:
        doxygen = LxmlHelper.objectify_xml(input_path_dir)
        namespace_definition_object = super().load(doxygen)
        if hasattr(doxygen.compounddef, "innerclass"):
            for inner in doxygen.compounddef.innerclass:
                inner_class_object = InnerClassTagNS()
                inner_class_object.reference_id = inner.get("refid")
                inner_class_object.protection = inner.get("prot")
                inner_class_object.text = inner.text
                namespace_definition_object.add_inner_class(inner_class_object)
            # END --- FOR...doxygen.compounddef.innerclass
        # END --- IF
        return namespace_definition_object

# class NamespaceDefinitionFactory:
#     @classmethod
#     def load_namespace(cls, doxygen):
#         kind = doxygen.compounddef.get("kind")
#         if kind == "namespace":
#             namespace_definition_object = NamespaceCompoundDefinition()
#             namespace_definition_object.id = doxygen.compounddef.get("id")
#             namespace_definition_object.kind = doxygen.compounddef.get("kind")
#             namespace_definition_object.language = doxygen.compounddef.get("language")
#             namespace_definition_object.compound_name = doxygen.compounddef.compoundname
#             # add <innerclass> tag info
#             if hasattr(doxygen.compounddef, "innerclass"):
#                 for inner in doxygen.compounddef.innerclass:
#                     inner_class_object = InnerClassTagNS()
#                     inner_class_object.reference_id = inner.get("refid")
#                     inner_class_object.protection = inner.get("prot")
#                     inner_class_object.text = inner
#                     namespace_definition_object.add_inner_class(inner_class_object)
#                 # END --- FOR...doxygen.compounddef.innerclass
#             # add rest of the tag info
#             namespace_definition_object.brief_description = doxygen.compounddef.briefdescription
#             namespace_definition_object.detailed_description = doxygen.compounddef.detaileddescription
#             # add location information
#             location_object = LocationDefinitionCL()
#             location_object.file = doxygen.compounddef.location.get("file")
#             location_object.line = doxygen.compounddef.location.get("line")
#             location_object.column = doxygen.compounddef.location.get("column")
#             namespace_definition_object.location = location_object
#
#             return namespace_definition_object
#
# if __name__ == "__main__":
#     import lxml.etree as etree
#     from lxml import objectify
#
#     from factories.NamespaceDefinitionFactory import NamespaceDefinitionFactory
#
#     dom = etree.parse("xmls/namespace.xml")  # parse xml and concert it into DOM
#     xmlBin = etree.tostring(dom, pretty_print=False, encoding="ascii")  # encode DOM into ASCII object
#     xmlText = xmlBin.decode()  # convert binary ASCII object into ASCII text
#     doxygen = objectify.fromstring(xmlText)  # convert text into a Doxygen object
#
#     ns_def = NamespaceDefinitionFactory.load_namespace(doxygen)
#     print(ns_def)
