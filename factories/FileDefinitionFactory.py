from definitions.ChildNodeTagBCL import ChildNodeTagBCL
from definitions.FileCompoundDefinition import FileCompoundDefinition
from definitions.IncludeDepGraphTagFL import IncludeDepGraphTagFL
from definitions.IncludesTagBCL import IncludesTagBCL
from definitions.InnerClassTagNS import InnerClassTagNS
from definitions.InnerNamespaceTag import InnerNamespaceTag
from definitions.LinkTagBCL import LinkTagBCL
from definitions.NodeTagBCL import NodeTagBCL


class FileDefinitionFactory:
    @classmethod
    def __add_includes_to_file_definition_object(cls, file_definition_object, compounddef):
        for incl in compounddef.includes:
            include_object = IncludesTagBCL()
            include_object.local = incl.get("local")
            include_object.name = incl
            file_definition_object.add_includes(include_object)

    @classmethod
    def __add_nodes_to_incdepgraph_object(cls, incdepgraph_object, incdepgraph):
        for nod in incdepgraph.node:
            node_object = NodeTagBCL()
            node_object.id = nod.get("id")
            node_object.label = nod.label
            # add link
            if hasattr(nod, "link"):
                link_object = LinkTagBCL()
                link_object.reference_id = nod.link.get("refid")
                node_object.link = link_object
            #end of if
            # add child nodes
            if hasattr(nod, "childnode"):
                for c_node in nod.childnode:
                    c_node_obj = ChildNodeTagBCL()
                    c_node_obj.reference_id = c_node.get("refid")
                    c_node_obj.relation = c_node.get("relation")
                    node_object.add_child_node(c_node_obj)
                # end of inner for loop
            #end of if
            # add the <node> object
            incdepgraph_object.add_node(node_object)
        # end of oute for loop

    @classmethod
    def __add_inner_classes_to_file_definition_object(cls, file_definition_object, compounddef):
        for inner_c in compounddef.innerclass:
            inner_class_object = InnerClassTagNS()
            inner_class_object.reference_id = inner_c.get("refid")
            inner_class_object.protection = inner_c.get("prot")
            inner_class_object.text = inner_c
            # add <innerclass>
            file_definition_object.add_inner_class(inner_class_object)

    @classmethod
    def load_file(cls, doxygen):
        kind = doxygen.compounddef.get("kind")
        if kind == "file":
            file_definition_object = FileCompoundDefinition()
            file_definition_object.id = doxygen.compounddef.get("id")
            file_definition_object.kind = doxygen.compounddef.get("file")
            file_definition_object.language = doxygen.compounddef.get("language")
            file_definition_object.compound_name = doxygen.compounddef.compoundname
            # add multiple <includes>
            if hasattr(doxygen.compounddef, "includes"):
                cls.__add_includes_to_file_definition_object(file_definition_object, doxygen.compounddef)

            # add one <incdepgraph> to file_definition_object
            if hasattr(doxygen.compounddef, "incdepgraph"):
                incdepgraph_object = IncludeDepGraphTagFL()
                cls.__add_nodes_to_incdepgraph_object(incdepgraph_object, doxygen.compounddef.incdepgraph)
                file_definition_object.include_dependency_graph = incdepgraph_object

            # add multiple <innerclass> tags to file_definition_object
            if hasattr(doxygen.compounddef, "innerclass"):
                cls.__add_inner_classes_to_file_definition_object(file_definition_object, doxygen.compounddef)

            # add one <innernamespace> tag to file_definition_object
            if hasattr(doxygen.compounddef, "innernamespace"):
                innernamespace_obj = InnerNamespaceTag()
                innernamespace_obj.reference_id = doxygen.compounddef.innernamespace.get("refid")
                innernamespace_obj.text = doxygen.compounddef.innernamespace
                # add one <innernamespace> tag to file_definition_object
                file_definition_object.inner_namespace = innernamespace_obj

            return file_definition_object
