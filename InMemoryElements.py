from Utils import FileExtension


class InMemoryElements:
    Is_File_Extension: bool = True
    Default_File_Extension: str = FileExtension.RST
    Is_Include_Private: bool = False
