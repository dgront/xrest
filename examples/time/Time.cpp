/ **@file Time.cpp
* /
//
// Created by Justyna Kry? on 29/11/2019.
//
#include <iostream>
#include <math.h>
/ **@namespace time
* Namespace for Time utilities
* /
namespace time
{
    /// Clock structure
    struct Clock
    {
        std::string color;

        void tick();
    };

    / **@brief A class representing time
        * /
        class Time
    {
    public:
        / **@brief Constructor - creates an object set to midnight(0:00)
            *
            *no argument
            * /
            Time();
        / **@brief Desktruktor
            *
            *Default destructor
            * /
            ~Time();
        / **@brief Copy constructor
            *
            *Long copy constructor
            * /
            Time(Time&);
            Time(int a);
        / **@brief creates an object at an equal time
            *
            *cos
            * @param hour
            * /
            Time(int hour);
        / **@brief creates an object with the given value
            *
            *dude
            * @param hour
            * @ a few minutes
            * /
            Time(int hour, int minutes);
        / **@brief prints the time in 24 - hour format
            *
            */
            void print24();
        / **@brief prints the time in 12 - hour format
            *
            */
            void print12();
        / **@brief advances the time by the number of hours and minutes
            *
            *moves the time by the set number of hours and minutes
            * @param how manyHours
            * @param ileMinut
            * @return reference to the object
            * /
            Time & add(int how_many_hours, int how_many_minutes);
        / **@brief returns the number of minutes before the given hour
            *
            *andI will write something else here
            * @param c
            * @return int
            * /
            int how_many_more(Time c);

    private:
        int minutes; /// <holds the number of minutes
    };

    Time::Time()
    {
        minutes = 0;
    }

    Time::Time(int hours)
    {
        minutes = (60 * hour) % 1440;
    }

    Time::Time(int hour, int minutes)
    {
        minutes = (60 * hour + minutes) % 1440;
    }

    void Time::print12()
    {
        std::cout << "12hrs:";
        int h = ceil(minutes / 60);
        if (minutes - h * 60 < 10)
        {
            std::cout << h % 12 << ": 0" << minutes - h * 60 << "\n";
        }
        else
        {
            std::cout << h % 12 << ":" << minutes - h * 60 << "\n";
        }
    }

    void Time::print24()
    {
        std::cout << "24hours:";
        int h = ceil(minutes / 60);
        if (minutes - h * 60 < 10)
        {
            std::cout << h << ": 0" << minutes - h * 60 << "\ n";
        }
        else
        {
            std::cout << h << ":" << minutes - h * 60 << "\ n";
        }
    }

    Time& Time::add(int how_many_hours, int how_many_minutes)
    {
        minutes + = how_many_hours * 60 + how_many_minutes;
        return *this;
    }

    int Time::how_many_more(Time c)
    {
        if (c.minutes < minutes)
        {
            return 1440 - minutes + c.minutes;
        }
        else
        {
            return c.minutes - minutes;
        }
    }
}