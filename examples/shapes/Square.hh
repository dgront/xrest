#ifndef SQUARE_hh
#define SQUARE_hh

#include "ShapeBase.hh"
#include <string>

namespace shapes {

/// Represents a square
class Square: public ShapeBase {
public:

  /** @brief Constructor sets fields of this object
   * @param id: id string of this shape
   * @param a: side width
   */
  Square(const std::string & id, const double a) : ShapeBase(id), a_(a) {}

  double area() const {  return a_*a_; }

  /// Returns width of this square
  double width() const { return a_; }

private:
  double a_;
};

}

#endif
