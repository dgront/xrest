/**
 * @file ShapeBase.hh
 * @brief provides ShapeBase base class
 */
#ifndef SHAPE_BASE
#define SHAPE_BASE


#include <string>

/**
 * @namespace shapes
 * @brief Holds a library of 2D shapes
 */
namespace shapes {

/// Base class for all 2D shapes
class ShapeBase {
public:
  /** @brief Base class constructor
   * @param id: id string of this shape
   */
  ShapeBase(const std::string & id) : id_(id) {}

  /// Returns id string of this shape
  const std::string & id() const { return id_; }

  /// Returns area of this shape
  virtual double area() const = 0;

  /// Returns X coordinate of the center of this shape
  double cx() { return cx_; }

  /// Returns Y coordinate of the center of this shape
  double cy() { return cy_; }

  /// Sets X coordinate of the center of this shape
  void cx(double x) { cx_ = x; }

  /// Sets Y coordinate of the center of this shape
  void cy(double y) { cy_ = y; }

protected:
  double cx_, cy_;
  
private:
  std::string id_;
};

}

#endif
