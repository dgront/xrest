#ifndef CIRCLE_hh
#define CIRCLE_hh

#include "ShapeBase.hh"
#include <string>
#include <cmath>

namespace shapes {

/// Represents a square
class Circle: public ShapeBase {
public:

  /** @brief Constructor sets fields of this object
   * @param id: id string of this shape
   * @param r: radius of the circle
   */
  Circle(const std::string & id, const double r) : ShapeBase(id), r_(r) {}

  double area() const {  return M_PI*r_*r_; }

  /// Returns the radius of this square
  double r() const { return r_; }

private:
  double r_;
};

}

#endif
