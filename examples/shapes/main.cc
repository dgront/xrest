#include <iostream>

#include "Square.hh"
#include "Circle.hh"

int main(int argc, char *argv[]) {

   shapes::Square s1("S1", 4);
   std::cout << s1.area()<<"\n";

   shapes::Circle c1("S1", 4), c2("S1", 2);
   std::cout << c1.area()<< " " << c2.area()<<"\n";
}
