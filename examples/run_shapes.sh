#!/bin/bash

# ---------- Remove outputs from a previous run
rm -rf shapes-html shapes-rst

# ---------- Run xrest
python3 ../xrest.py -i shapes-dox/ -o shapes-rst --rst

# ---------- Provide Makefile and conf.py that are necessary for Sphinx run
cp ../templates/conf.py ./shapes-rst
cp ../templates/Makefile ./

# ---------- Run Sphinx!
make html
