#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Author: Mohammad Saqib
from typing import Dict, List
from argparse import ArgumentParser
from ClassStructDictElement import ClassStructDictElement
from FileIO import FileIO
from InMemoryElements import InMemoryElements
from PathValidation import PathValidation
from SearchFilesInTheDirectory import SearchFilesInTheDirectory
from SystemUtils import SystemUtils
from Utils import BColors, Utils, FileExtension, XMLTypes
from definitions.IndexIN import IndexIN
from definitions.NamespaceCompoundDefinition import NamespaceCompoundDefinition
from factories.ClassStructCompounddefinitionFactory import ClassStructCompoundDefinitionFactory
from factories.NamespaceCompoundDefinitionFactory import NamespaceCompoundDefinitionFactory


class MainClass:
    # <editor-fold desc="(path strings)">
    ###__default_input_path_str = r"C:\Users\pc\source\repos\BitBucket\MyClass___xml\xml"
    __default_input_path_str = r"C:\Users\pc\source\repos\BitBucket\Attributes___duplicate___xml\xml"
    __default_output_path_str: str = r"C:\Users\pc\Desktop\XREST_____rst\src"
    ### output_path = r"C:\Users\pc\source\repos\BitBucket\BioShell.Xml.3\txt"
    __default_file_extension_str: str = FileExtension.RST
    __include_private_bool: bool = True
    __is_include_file_extension: bool = True
    # </editor-fold>

    # <editor-fold desc="(dictionaries for creating index)">
    __class_dictionary: Dict[str, ClassStructDictElement] = {}
    __namespace_dictionary: Dict[str, NamespaceCompoundDefinition] = {}
    __index_list: List[str] = []
    # </editor-fold>

    # <editor-fold desc="(__parse_args())">
    @classmethod
    def __parse_args(cls):
        # initialize argument parser
        my_parser = ArgumentParser()
        my_parser.add_argument("-i", help="input directory", type=str)
        my_parser.add_argument("-o", help="output directory", type=str)
        my_parser.add_argument("--include_private", help="add private variables to the output", action='store_true')
        my_group = my_parser.add_mutually_exclusive_group()
        my_group.add_argument("--txt", help="output as .txt files", action='store_true')
        my_group.add_argument("--rst", help="output as .rst files", action='store_true')

        # parse the argument
        args = my_parser.parse_args()

        if args.i is not None:
            cls.__default_input_path_str = args.i
        if args.o is not None:
            cls.__default_output_path_str = args.o
        if args.include_private is True:
            cls.__include_private_bool = True
        if args.rst is True:
            cls.__default_file_extension_str = FileExtension.RST
        if args.txt is True:
            cls.__default_file_extension_str = FileExtension.TXT
    # </editor-fold>

    # <editor-fold desc="(__write_rst())">
    @classmethod
    def __write_rst(cls, def_obj, xmlType):
        try:
            location_obj = def_obj.get_location()
            if PathValidation.is_pathname_valid(str(location_obj.file)):
                ## file_path_str = SystemUtils.get_dir(str(location_obj.file))
                new_output_path_str = cls.__default_output_path_str ### SystemUtils.merge_paths(cls.__default_output_path_str, file_path_str)
                new_file_name_str = def_obj.get_file_name_to_write_on()

                new_file_name_str = Utils.concat(new_file_name_str, ".", cls.__default_file_extension_str)
                if xmlType == XMLTypes.CLASS:
                    new_file_name_str += ".inc"
                # END if.

                def_obj.process() # generate all necessary texts and objects from a class/namespace definition

                text_to_write_str = def_obj.get_rst() # get rst file for the CLASS
                print("Writing [" + SystemUtils.merge_paths(new_output_path_str, new_file_name_str) + "]")
                FileIO.write_to_file(new_output_path_str, new_file_name_str, text_to_write_str)  # write class rst

                func_rst_dictionary = def_obj.get_individual_functions_rst() # get rst file for the FUNCTIONS
                for func_key_str in func_rst_dictionary:  # write class-members rst
                    new_file_name_str = Utils.concat(func_key_str, ".", cls.__default_file_extension_str, ".inc")
                    print("Writing [" + SystemUtils.merge_paths(new_output_path_str, new_file_name_str) + "]")
                    temp_text = func_rst_dictionary[func_key_str]
                    FileIO.write_to_file(new_output_path_str, new_file_name_str, temp_text)
                # END of ... for
            else:
                location_str = str(def_obj.location.file)
                error_str = "XREST Error: The location info [" + location_str + "] is invalid."
                print(BColors.FAIL + error_str + BColors.ENDC)
            # END of ... if
        except Exception as e:
            print(BColors.FAIL + "[" + str(e) + "]" + BColors.ENDC)
        # END of ... try-except
    # END of ... __write_rst()
    # </editor-fold>

    # <editor-fold desc="(__process_class_xmls())">
    @classmethod
    def __process_class_xmls(cls, is_write: bool, is_messages: bool):
        try:
            class_factory = ClassStructCompoundDefinitionFactory()  # create a class-factory object
            class_path_list = SearchFilesInTheDirectory.get_class_file_paths_list()
            if class_path_list is None:
                return
            elif len(class_path_list) <= 0:
                return

            for class_path in class_path_list:
                if is_messages:
                    print(BColors.BOLD + "Reading class [" + class_path + "]" + BColors.ENDC)
                # END of ... if
                class_def = class_factory.load(class_path)  # create object from XML file
                if class_def == None:
                    continue

                if is_messages:
                    print_str = BColors.BOLD + class_def.compound_name + " loaded as type: " \
                                + str(type(class_def)) + BColors.ENDC
                    print(print_str)

                if class_def is not None:
                    namespace_name_str = class_def.get_namespace_name_as_key()
                    location_str = class_def.get_dir()
                    brief_desc_obj = class_def.get_brief_description()
                    brief_desc_text = brief_desc_obj.text
                    class_item_obj = ClassStructDictElement(class_def.get_file_name_to_write_on(),
                                                            class_def.compound_name,
                                                            namespace_name_str,
                                                            location_str,
                                                            brief_desc_text)
                    class_name_as_key = class_def.get_file_name_to_write_on()
                    cls.__class_dictionary[class_name_as_key] = class_item_obj  # store directory path in the dictionary
                    if is_write:
                        cls.__write_rst(class_def, XMLTypes.CLASS)
                    # END of ... if
                else:
                    error_str = "XREST Error: The file [" + class_path + "] could not be de-serialized."
                    print(BColors.FAIL + error_str + BColors.ENDC)
                # END of if-else
            # END of for
            print(BColors.BOLD + "completed processing classes" + BColors.ENDC)
        except Exception as e:
            print(BColors.FAIL + str(e) + BColors.ENDC)
        # END of ... try-except
    # END of ... __process_class_xmls()
    # </editor-fold>

    # <editor-fold desc="(__process_namespace_xmls())">
    @classmethod
    def __process_namespace_xmls(cls, is_write: bool, is_messages: bool):
        try:
            namespace_factory = NamespaceCompoundDefinitionFactory() # create a class-factory object
            namespace_path_list = SearchFilesInTheDirectory.get_namespace_file_paths_list() # obtain all the paths of namespace XML files
            # if there are no XML files related to namespaces, return
            if namespace_path_list is None:
                return
            elif len(namespace_path_list) <= 0:
                return
            # END of ... IF
            NamespaceCompoundDefinition.set_class_dictionary(cls.__class_dictionary) # store the class-dictionary as as a class-attributes
            NamespaceCompoundDefinition.set_file_extension(cls.__default_file_extension_str) # store the file extension as as a class-attributes
            # iterate through all the namespace XML paths
            for namespace_path in namespace_path_list:
                if is_messages:
                    print(BColors.BOLD + "Reading namespace [" + namespace_path + "]" + BColors.ENDC)
                # END of ... if
                # load an XML into memory
                namespace_def_obj = namespace_factory.load(namespace_path)  # create object from XML file
                if namespace_def_obj is not None:
                    namespace_name_as_key = namespace_def_obj.get_file_name_to_write_on()
                    cls.__namespace_dictionary[namespace_name_as_key] = namespace_def_obj # store the namespace_object in the dictionary
                    ##############################################################################
                    # write namespace and its members
                    if is_write:
                        cls.__write_rst(namespace_def_obj, XMLTypes.NAMESPACE)
                    ##############################################################################
                    # write index file for a namespace
                    output_dir_str = cls.__default_output_path_str ###SystemUtils.merge_paths(cls.__default_output_path_str, namespace_def_obj.get_dir())
                    name_str = Utils.concat("index_", namespace_name_as_key)
                    index_file_name_str = Utils.concat(name_str, ".", cls.__default_file_extension_str)
                    text_str = namespace_def_obj.get_local_index_rst()
                    ## index_dir_str = namespace_def_obj.get_dir()
                    ##index_merged_path_str = SystemUtils.merge_paths(index_dir_str, name_str)
                    cls.__index_list.append(name_str)
                    FileIO.write_to_file(output_dir_str, index_file_name_str, text_str)
                    ##############################################################################
                else:
                    error_str = "XREST Error: The file [" + namespace_path + "] could not be de-serialized."
                    print(BColors.FAIL + error_str + BColors.ENDC)
                # END of ... if
            # END of ... for loop
            print(BColors.BOLD + "completed processing namespaces" + BColors.ENDC)
        except Exception as e:
            print(str(e))
        # END of ... try-except
    # END of ... __process_namespace_xmls()
    # </editor-fold>

    # <editor-fold desc="(__process_main_index())">
    @classmethod
    def __process_main_index(cls):
        if len(cls.__index_list) <= 0:
            return

        index_obj = IndexIN()
        text_to_write_str = index_obj.get_main_index_rst(cls.__index_list, cls.__default_file_extension_str)
        file_name_str = Utils.concat("index", ".", cls.__default_file_extension_str)
        FileIO.write_to_file(cls.__default_output_path_str, file_name_str, text_to_write_str)
        print(BColors.BOLD + "completed outputting index" + BColors.ENDC)
        print(BColors.OKBLUE + "Please, move the \'index.rst\' file to the Sphinx-doc's root directory." + BColors.ENDC)
    # </editor-fold>

    # <editor-fold desc="(main())">
    @classmethod
    def main(cls):
        # parse command-line arguments
        cls.__parse_args()
        # only do indexing of XML files
        SearchFilesInTheDirectory.do_indexing_of_files(path_str=cls.__default_input_path_str, file_extension="xml", is_messages=True)

        # store some lightweight values in the memory
        InMemoryElements.Is_File_Extension = cls.__is_include_file_extension
        InMemoryElements.Default_File_Extension = cls.__default_file_extension_str
        InMemoryElements.Is_Include_Private = cls.__include_private_bool

        # clear the destination folder
        ## FileIO.delete_contents(cls.__default_output_path_str) ##

        # process XML files...
        cls.__process_class_xmls(is_write=True, is_messages=False)  # set as `False` for debugging purposes
        cls.__process_namespace_xmls(is_write=True, is_messages=False)  # set as `False` for debugging purposes
        cls.__process_main_index()
    # END of ... main()
    # </editor-fold>

# END of ... MainClass


if __name__ == '__main__':
    MainClass.main()
# END of ... if
