from enum import Enum


class TagTypeEnum(Enum):
    NoType = 0
    PlainText = 1
    Formula = 2
    SimpleSect = 3
    ParameterList = 4
    Ref = 5
    Bold = 6
    ComputerOutput = 7

