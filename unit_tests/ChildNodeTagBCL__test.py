import unittest
from definitions.ChildNodeTagBCL import ChildNodeTagBCL


class ChildNodeTagBCL_test(unittest.TestCase):
    def test_reference_id(self):
        child_node_object = ChildNodeTagBCL()
        child_node_object.reference_id = "ref-id"
        child_node_object.relation = "relation"

        self.assertEqual(child_node_object.reference_id, "ref-id")
        self.assertEqual(child_node_object.relation, "relation")

