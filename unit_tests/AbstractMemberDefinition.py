import unittest
from definitions.ChildNodeTagBCL import ChildNodeTagBCL
from definitions.LinkTagBCL import LinkTagBCL


class AbstractMemberDefinition_test(unittest.TestCase):
    def test_reference_id(self):
        link_object = LinkTagBCL()
        link_object.reference_id = "ref-id"

        self.assertEqual(link_object.reference_id, "ref-id")

