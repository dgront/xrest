import unittest
from definitions.AbstractCompoundDefinition import AbstractCompoundDefinition
from definitions.LinkTagBCL import LinkTagBCL


class AbstractCompoundDefinition_test(unittest.TestCase):
    def test_reference_id(self):
        my_object = AbstractCompoundDefinition()
        my_object.id = "id"
        my_object.kind = "kind"
        my_object.compound_name = "compound"

        self.assertEqual(my_object.id, "id")
        self.assertEqual(my_object.kind, "kind")
        self.assertEqual(my_object.compound_name, "compound")

