import unittest

from definitions.BriefDescription import BriefDescription
from definitions.ChildNodeTagBCL import ChildNodeTagBCL
from definitions.LinkTagBCL import LinkTagBCL


class BriefDescription_test(unittest.TestCase):
    def test_reference_id(self):
        link_object = BriefDescription()
        link_object.reference_id = "ref-id"

        self.assertEqual(link_object.reference_id, "ref-id")

