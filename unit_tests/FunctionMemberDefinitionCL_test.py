import unittest
from definitions.FunctionMemberDefinitionCL import FunctionMemberDefinitionCL


class FunctionMemberDefinitionCL_test(unittest.TestCase):
    def test_rst(self):
        my_object = FunctionMemberDefinitionCL()
        my_object.id = "id"
        my_object.kind = "kind"
        my_object.compound_name = "compound"
        my_object.virtual = ""
        my_object.args_string = "int, int"
        my_object.definition = "function_definition"
        my_object.name = "function_name"

        print(my_object.get_rst())
        # self.assertEqual(my_object.id, "id")

