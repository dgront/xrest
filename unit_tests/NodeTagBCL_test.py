import unittest
from definitions.ChildNodeTagBCL import ChildNodeTagBCL
from definitions.LinkTagBCL import LinkTagBCL
from definitions.NodeTagBCL import NodeTagBCL


class NodeTagBCL_test(unittest.TestCase):
    def test_object(self):
        link_object = LinkTagBCL()
        link_object.reference_id = "ref-id"

        child_node_object = ChildNodeTagBCL()
        child_node_object.reference_id = "ref-id"
        child_node_object.relation = "relation"

        node_object = NodeTagBCL()
        node_object.id = "id"
        node_object.label = "label"
        node_object.set_link(link_object)
        node_object.add_child_node(child_node_object)

        self.assertEqual(node_object.get_link().reference_id, "ref-id")
        self.assertEqual(node_object.get_child_nodes_list().pop(0).reference_id, "ref-id")

