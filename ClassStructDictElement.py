from SystemUtils import SystemUtils
from Utils import Utils


class ClassStructDictElement:
    __unique_file_name: str
    __class_name: str
    __namespace_name: str
    __path: str
    __brief_description: str

    def __init__(self, unique_file_name, class_name, namespace_name, path, brief_description):
        self.__unique_file_name = unique_file_name
        self.__class_name = class_name
        self.__namespace_name = namespace_name
        self.__path = path
        self.__brief_description = brief_description

    @property
    def unique_file_name(self):
        return self.__unique_file_name

    @unique_file_name.setter
    def unique_file_name(self, t):
        self.__unique_file_name = t

    @property
    def class_name(self):
        return self.__class_name.replace("<", "\<").replace(">", "\>")

    @class_name.setter
    def class_name(self, t):
        self.__class_name = t

    @property
    def namespace_name(self):
        return self.__namespace_name

    @namespace_name.setter
    def namespace_name(self, t):
        self.__namespace_name = t

    @property
    def path(self):
        return self.__path

    @path.setter
    def path(self, t):
        self.__path = t

    @property
    def brief_description(self):
        return self.__brief_description

    @brief_description.setter
    def brief_description(self, t):
        self.__brief_description = t

    def get_full_path(self, file_extension_str: str):
        fine_name_str = Utils.concat(self.unique_file_name, ".", file_extension_str)
        return SystemUtils.merge_paths(self.path, fine_name_str)
