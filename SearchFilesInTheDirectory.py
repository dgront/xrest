import os
from typing import List
from FileIO import FileIO
from SystemUtils import SystemUtils
from Utils import XMLTypes, Utils


class SearchFilesInTheDirectory:
    __namespace_file_paths_list: List[str] = []
    __class_file_paths_list: List[str] = []
    __other_file_paths_list: List[str] = []

    # def __init__(self):
    #     self.__namespace_file_paths_list = []
    #     self.__class_file_paths_list = []
    #     self.__other_file_paths_list = []

    @classmethod
    def get_namespace_file_paths_list(cls):
        return cls.__namespace_file_paths_list

    @classmethod
    def get_class_file_paths_list(cls):
        return cls.__class_file_paths_list

    @classmethod
    def get_other_file_paths_list(cls):
        return cls.__other_file_paths_list

    @classmethod
    def __process_path(cls, file_path):
        n_lines = 3  # read first three lines
        three_lines_of_text = FileIO.read_first_n_lines(file_path, n_lines)
        if XMLTypes.NAMESPACE in three_lines_of_text:
            cls.__namespace_file_paths_list.append(file_path)
        elif XMLTypes.CLASS in three_lines_of_text:
            cls.__class_file_paths_list.append(file_path)
        elif XMLTypes.STRUCT in three_lines_of_text:
            cls.__class_file_paths_list.append(file_path)
        else:
            cls.__other_file_paths_list.append(file_path)

    @classmethod
    def do_indexing_of_files(cls, path_str, file_extension, is_messages):
        try:
            if not FileIO.path_exists(path_str):
                return

            if SystemUtils.is_file_str(path_str):
                cls.__process_path(path_str)
                return

            file_extension = Utils.concat(".", file_extension)

            for root, dirs, files in os.walk(path_str):
                for file in files:
                    if file.endswith(file_extension):
                        file_path = os.path.join(root, file)
                        if is_messages:
                            print("Indexing [" + file_path + "]")
                        #END of if
                        cls.__process_path(file_path)

        except Exception as e:
            print(str(e))
        # END of ... try-except
    # END of ... do_indexing_of_files()


    #def __str__(self):
    #   return "\n".join(str(x) for x in [self.__dir_name, self.__file_extension, self.__file_list])
