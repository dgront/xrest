from typing import List
from jinja2 import Template

from Utils import Utils
from definitions.AbstractNonEnumMemberDefinition import AbstractNonEnumMemberDefinition
from definitions.ParamTagCL import ParamTagCL


# this class represents a <memberdef> tag where kind = "function"
class FunctionMemberDefinitionCL(AbstractNonEnumMemberDefinition):
    # <editor-fold desc="(constructors)">
    def __init__(self):
        super().__init__()
        self.__constant: str = None
        self.__explicit: str = None
        self.__inline: str = None
        self.__virtual: str = None
        self.__parameter_list: List[ParamTagCL] = []
    # </editor-fold>

    # <editor-fold desc="(properties)">
    @property
    def constant(self) -> str:
        return self.__constant

    @constant.setter
    def constant(self, constant: str):
        self.__constant = constant

    @property
    def explicit(self) -> str:
        return self.__explicit

    @explicit.setter
    def explicit(self, explicit: str):
        self.__explicit = explicit

    @property
    def inline(self) -> str:
        return self.__inline

    @inline.setter
    def inline(self, inline: str):
        self.__inline = inline
    @property
    def virtual(self) -> str:
        return self.__virtual

    @virtual.setter
    def virtual(self, virtual: str):
        self.__virtual = virtual
    # </editor-fold>

    def get_name_and_args_string(self):
        temp_str = self.name + " " + self.args_string
        # if last character is a white space, remove it
        if temp_str[-1] == ' ':
            temp_str = temp_str[:-1]
        return temp_str.replace("<", "\<").replace(">", "\>")

    def get_short_definition(self):
        return_type = ""
        if self.data_type is not None:
            return_type = self.data_type + " "
        temp_str = return_type + self.name + self.args_string
        return temp_str

    def get_long_definition(self):
        str_list = ['(', ')']
        #temp_str = ""
        #if self.virtual == "virtual":
        #    temp_str = self.virtual + " "
        temp_str = Utils.replace_chars(self.definition, str_list, "") + " " + self.args_string
        return temp_str

    # <editor-fold desc="(list accessor)">
    def add_parameter(self, parameter: ParamTagCL):
        if parameter:
            self.__parameter_list.append(parameter)

    def get_parameter_list(self) -> List[ParamTagCL]:
        return self.__parameter_list
    # </editor-fold>

    def get_file_name_to_write_on(self):
        return "function_" + super().get_file_name_to_write_on()

    # <editor-fold desc="(to string)">
    def __str__(self) -> str:
        out = super().__str__()  # print abstract class attributes

        if self.__constant is not None:
            out = out + "\n" + str(self.__constant)

        if self.__explicit is not None:
            out = out + "\n" + str(self.__explicit)

        if self.__inline is not None:
            out = out + "\n" + str(self.__inline)

        if self.__virtual is not None:
            out = out + "\n" + str(self.__virtual)

        if self.__parameter_list is not None:
            for param in self.__parameter_list:
                if param is not None:
                    out += "\n" + str(param)

        return out
    # </editor-fold>

    def get_rst(self):
        parameter_replacements = []

        for p in self.get_parameter_list():
            if hasattr(p, "declaration_name"):
                param_str = str(p.declaration_name)
                desc_obj = self.get_detailed_description()
                desc_str = desc_obj.get_description(param_str)
                parameter_replacements.append({"PARAMETER_NAME": param_str,
                                                "PARAMETER_TYPE": str(p.type_name),
                                                "PARAMETER_DESCRIPTION": desc_str})
            # END of if
        # END of ... for loop

        detailed_desc_str = self.get_detailed_description()

        replacements = {"HEADER_LINK": self.file_name_overload,
                        "FUNCTION_SIGNATURE": self.get_short_definition(),
                        "FUNCTION_FULL_SIGNATURE": self.get_long_definition(),
                        "BRIEF_DESCRIPTION": str(self.get_brief_description()),
                        "FUNCTION_ARGUMENTS": parameter_replacements,
                        "FULL_DESCRIPTION": str(detailed_desc_str.text)}

        tmplt = Template(FunctionMemberDefinitionCL.__rst_function_template)

        output = tmplt.render(replacements)
        ##print(output)
        return output

################################# Don't change the indentation #######################################################
    __rst_function_template = """.. _{{HEADER_LINK}}:

.. cpp:function:: {{FUNCTION_FULL_SIGNATURE}}

  {{BRIEF_DESCRIPTION}}
  
  {% for arg in FUNCTION_ARGUMENTS %}
  :param {{arg.PARAMETER_NAME}}: (``{{arg.PARAMETER_TYPE}}``)   {{arg.PARAMETER_DESCRIPTION}}
  {% endfor %}

  {{FULL_DESCRIPTION}}

"""
################################# Don't change the indentation #######################################################