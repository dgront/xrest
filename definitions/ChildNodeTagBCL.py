

class ChildNodeTagBCL:
    def __init__(self):
        self.__reference_id: str = None
        self.__relation: str = None

    @property
    def reference_id(self) -> str:
        return self.__reference_id

    @reference_id.setter
    def reference_id(self, reference_id: str):
        self.__reference_id = reference_id

    @property
    def relation(self) -> str:
        return self.__relation

    @relation.setter
    def relation(self, relation: str):
        self.__relation = relation

    def printf(self):
        print(self.__reference_id)
        print(self.__relation)

# <editor-fold desc="(to string)">
    def __str__(self) -> str:
        out = ""

        if self.__reference_id is not None:
            out += "\n" + str(self.__reference_id)

        if self.__relation is not None:
            out += "\n" + str(self.__relation)

        return out
    # </editor-fold>