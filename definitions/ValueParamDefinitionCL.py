from definitions.ParamTagCL import ParamTagCL


# this class represents a <param> tag in the xml
class ValueParamDefinitionCL(ParamTagCL):
    __declaration_name: str

    def __init__(self):
        super().__init__()
        self.__declaration_name = None

    @property
    def declaration_name(self) -> str:
        return self.__declaration_name

    @declaration_name.setter
    def declaration_name(self, declaration_name: str):
        self.__declaration_name = declaration_name

    def __str__(self) -> str:
        out = super().__str__()

        if self.__declaration_name is not None:
            out += "\n" + str(self.__declaration_name)

        return out



