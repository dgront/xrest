from typing import List

from TagTypeEnum import TagTypeEnum
from definitions.AbstractDescriptionItemBase import AbstractDescriptionItemBase


class BoldTag(AbstractDescriptionItemBase):
    def __init__(self):
        super().__init__()
        self._tag_type = TagTypeEnum.Bold
        self.__text_list: List[str] = []

    def get_item_list(self):
        return self.__text_list

    def add_item(self, t):
        self.__text_list.append(t)

    def __str__(self):
        out = super().__str__()

        for sss in self.__text_list:
            out += "\n" + sss

        return out