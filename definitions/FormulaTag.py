from typing import List

from TagTypeEnum import TagTypeEnum
from definitions.AbstractDescriptionItemBase import AbstractDescriptionItemBase


class FormulaTag(AbstractDescriptionItemBase):
    def __init__(self):
        super().__init__()
        self._tag_type = TagTypeEnum.Formula
        self.__id: str = None
        self.__text_list: List[str] = []

    @property
    def id(self) -> str:
        return self.__id

    @id.setter
    def id(self, id: str):
        self.__id = id

    def add_item(self, text: str):
        self.__text_list.append(text)

    def get_item_list(self) -> List[str]:
        return self.__text_list

    # <editor-fold desc="(to string)">
    def __str__(self):
        out = ""

        for sss in self.__text_list:
            out += "\n" + sss

        return out

    # </editor-fold>

    @classmethod
    def get_test_object(cls):
        p = FormulaTag()
        p.text = "hello!"
        return p