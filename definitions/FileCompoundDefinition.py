from typing import List, Optional

from definitions.AbstractCompoundDefinition import AbstractCompoundDefinition

# this class represents a <compountdef> tag in the xml where kind="file".
from definitions.BriefDescription import BriefDescription
from definitions.ClassStructCompoundDefinition import ClassStructCompoundDefinition
from definitions.DetailedDescription import DetailedDescription
from definitions.FunctionMemberDefinitionCL import FunctionMemberDefinitionCL
from definitions.IncludeDepGraphTagFL import IncludeDepGraphTagFL
from definitions.IncludesTagBCL import IncludesTagBCL
from definitions.InnerClassTagNS import InnerClassTagNS
from definitions.InnerNamespaceTag import InnerNamespaceTag
from definitions.LocationDefinitionCL import LocationDefinitionCL
from definitions.ParamTagCL import ParamTagCL
from definitions.SectionDefinitionCL import SectionDefinitionCL
from definitions.VariableMemberDefinitionCL import VariableMemberDefinitionCL


class FileCompoundDefinition(AbstractCompoundDefinition):
    # <editor-fold desc="(constructor)">
    def __init__(self):
        super().__init__()
        self.__language: str = None
        self.__includes_list: List[IncludesTagBCL] = []
        self.__inner_class_list: List[InnerClassTagNS] = []
        self.__include_dependency_graph: IncludeDepGraphTagFL = None
        self.__inner_namespace: InnerNamespaceTag = None
        self.__brief_description: BriefDescription = None
        self.__detailed_description: DetailedDescription = None
        self.__location: LocationDefinitionCL = None
    # </editor-fold>

    # <editor-fold desc="(properties)">
    @property
    def language(self) -> str:
        return self.__language

    @language.setter
    def language(self, language: str):
        self.__language = language

    # </editor-fold>

    # <editor-fold desc="(list accessors)">
    def add_includes(self, include: IncludesTagBCL):
        if include is not None:
            self.__includes_list.append(include)

    def get_includes_list(self):
        return self.__includes_list

    def add_inner_class(self, inner_class: InnerClassTagNS):
        if inner_class is not None:
            self.__inner_class_list.append(inner_class)

    def get_inner_class_list(self) -> List[InnerClassTagNS]:
        return self.__inner_class_list

    # </editor-fold>

    # <editor-fold desc="(object accessors)">
    def set_include_dependency_graph(self, include_dependency_graph: IncludeDepGraphTagFL):
        self.__include_dependency_graph = include_dependency_graph

    def get_include_dependency_graph(self) -> IncludeDepGraphTagFL:
        return self.__include_dependency_graph

    def set_inner_namespace(self, inner_namespace: InnerNamespaceTag):
        self.__inner_namespace = inner_namespace

    def get_inner_namespace(self) -> InnerNamespaceTag:
        return self.__inner_namespace

    def set_brief_description(self, brief_description: BriefDescription):
        self.__brief_description = brief_description

    def get_brief_description(self) -> BriefDescription:
        return self.__brief_description

    def set_detailed_description(self, detailed_description: DetailedDescription):
        self.__detailed_description = detailed_description

    def get_detailed_description(self) -> DetailedDescription:
        return self.__detailed_description

    def set_location(self, location: LocationDefinitionCL):
        self.__location = location

    def get_location(self) -> LocationDefinitionCL:
        return self.__location
    # </editor-fold>

# self.__language = None
# self.__include_dependency_graph = None
# self.__inner_namespace = None
# self.__brief_description = None
# self.__detailed_description = None
# self.__location = None

    # <editor-fold desc="(to string)">
    def __str__(self) -> str:
        out = super().__str__()  # print abstract class attributes

        if self.__language is not None:
            out += "\n" + str(self.__language)

        if self.__include_dependency_graph is not None:
            out += "\n" + str(self.__include_dependency_graph)

        if self.__inner_namespace is not None:
            out += "\n" + str(self.__inner_namespace)

        if self.__brief_description is not None:
            out += "\n" + str(self.__brief_description)

        if self.__detailed_description is not None:
            out += "\n" + str(self.__detailed_description)

        if self.__location is not None:
            out += "\n" + str(self.__location)

        if self.__includes_list is not None:
            for sec in self.__includes_list:
                if sec is not None:
                    out += "\n" + str(sec)

        if self.__inner_class_list is not None:
            for sec in self.__inner_class_list:
                if sec is not None:
                    out += "\n" + str(sec)

        return out
    # </editor-fold>



