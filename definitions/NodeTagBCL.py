from typing import List, Optional

from definitions.ChildNodeTagBCL import ChildNodeTagBCL
from definitions.LinkTagBCL import LinkTagBCL


class NodeTagBCL:
    __id: str
    __label: str
    __link: LinkTagBCL
    __child_node_list: List[ChildNodeTagBCL]

    def __init__(self):
        self.__id = None
        self.__label = None
        self.__link = None
        self.__child_node_list = []

    @property
    def id(self) -> str:
        return self.__id

    @id.setter
    def id(self, id: str):
        self.__id = id

    @property
    def label(self) -> str:
        return self.__label

    @label.setter
    def label(self, label: str):
        self.__label = label

    def get_link(self) -> LinkTagBCL:
        return self.__link

    def set_link(self, link: LinkTagBCL):
        self.__link = link

    def add_child_node(self, child_node: ChildNodeTagBCL):
        self.__child_node_list.append(child_node)

    def get_child_nodes_list(self) -> List[ChildNodeTagBCL]:
        return self.__child_node_list

    # <editor-fold desc="(to string)">
    def __str__(self) -> str:
        out = ""

        if self.__id is not None:
            out += "\n" + str(self.__id)

        if self.__label is not None:
            out += "\n" + str(self.__label)

        if self.__link is not None:
            out += "\n" + str(self.__link)

        if self.__child_node_list is not None:
            for sec in self.__child_node_list:
                if sec is not None:
                    out += "\n" + str(sec)

        return out
    # </editor-fold>