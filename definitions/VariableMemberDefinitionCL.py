from definitions.AbstractNonEnumMemberDefinition import AbstractNonEnumMemberDefinition
from definitions.LocationDefinitionCL import LocationDefinitionCL


# this class represents a <memberdef> tag where kind = "variable"
class VariableMemberDefinitionCL(AbstractNonEnumMemberDefinition):
    __mutable: str

    def __init__(self):
        super().__init__()
        self.__mutable = None

    @property
    def mutable(self) -> str:
        return self.__mutable

    @mutable.setter
    def mutable(self, mutable: str):
        self.__mutable = mutable

    def get_file_name_to_write_on(self):
        return super().get_file_name_to_write_on()

    def __str__(self) -> str:
        out = super().__str__()

        if self.__mutable is not None:
            out += "\n" + str(self.__mutable)

        return out