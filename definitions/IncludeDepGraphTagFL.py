from typing import List

from definitions.NodeTagBCL import NodeTagBCL


class IncludeDepGraphTagFL:
    def __init__(self):
        self.__nodes_list: List[NodeTagBCL] = []

    def add_node(self, node: NodeTagBCL):
        self.__nodes_list.append(node)

    def get_nodes_list(self, node) -> List[NodeTagBCL]:
        return self.__nodes_list

# <editor-fold desc="(to string)">
    def __str__(self) -> str:
        out = "\n"

        if self.__nodes_list is not None:
            for sec in self.__nodes_list:
                out += "\n" + str(sec)

        return out
    # </editor-fold>