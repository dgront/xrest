from typing import List, Dict

from InMemoryElements import InMemoryElements
from PathValidation import PathValidation
from SystemUtils import SystemUtils
from Utils import Utils
from definitions.AbstractMemberDefinition import AbstractMemberDefinition
from definitions.BriefDescription import BriefDescription
from definitions.DetailedDescription import DetailedDescription
from definitions.LocationDefinitionCL import LocationDefinitionCL
from definitions.SectionDefinitionCL import SectionDefinitionCL
from abc import ABC, abstractmethod

# there are
# this class represents a <compountdef> tag in the class.xml.
class AbstractCompoundDefinition(ABC):
    # <editor-fold desc="(constructor)">
    def __init__(self):
        self.__id: str = None
        self.__kind: str = None
        self.__compound_name: str = None
        self.__language: str = None
        #------------------------------
        self.__brief_description: BriefDescription = None
        self.__detailed_description: DetailedDescription = None
        self.__location: LocationDefinitionCL = None
        self.__section_def_list: List[SectionDefinitionCL] = []
        self.__member_def_list: List[AbstractMemberDefinition] = []
        # ---------------------------------------------------------
        # <editor-fold desc="(local computations)">
        # attributes for local operations
        self.__max_length_of_variable_name_int = 0
        self.__max_length_of_variable_desc_int = 0
        self.__max_length_of_function_name_int = 0
        self.__max_length_of_function_desc_int = 0
        self.__attr_dictionary = {}
        self.__function_dictionary = {}
        self.__function_rst_dictionary = {}
        self.__class_rst = ""
        self._bottom_links_str = ""
        # </editor-fold>
    # </editor-fold>

    # <editor-fold desc="(properties)">
    @property
    def id(self) -> str:
        return self.__id

    @id.setter
    def id(self, id: str):
        self.__id = id

    @property
    def kind(self) -> str:
        return self.__kind

    @kind.setter
    def kind(self, kind: str):
        self.__kind = kind

    @property
    def compound_name(self) -> str:
        return self.__compound_name

    @compound_name.setter
    def compound_name(self, compound_name: str):
        self.__compound_name = compound_name

    def get_namespace_name(self):
        full_name_str = self.__compound_name
        namespace_str_list = full_name_str.split("::")
        namespace_str_list.pop()
        namespace_name_str = "::".join(namespace_str_list)
        return namespace_name_str

    @property
    def language(self) -> str:
        return self.__language

    @language.setter
    def language(self, language: str):
        self.__language = language
    # </editor-fold>

    # <editor-fold desc="(object accessor)">
    def set_brief_description(self, brief_description: BriefDescription):
        self.__brief_description = brief_description

    def get_brief_description(self) -> BriefDescription:
        return self.__brief_description

    def set_detailed_description(self, detailed_description: DetailedDescription):
        self.__detailed_description = detailed_description

    def get_detailed_description(self) -> DetailedDescription:
        return self.__detailed_description

    def set_location(self, location: LocationDefinitionCL):
        self.__location = location

    def get_location(self) -> LocationDefinitionCL:
        temp_obj = self.__location
        return temp_obj

    def get_dir(self) -> str:
        location_obj = self.get_location()
        if PathValidation.is_pathname_valid(str(location_obj.file)):
            file_path_str = SystemUtils.get_dir(str(location_obj.file))
            return file_path_str
        # END of ... if
        return ""
    # END of get_dir()
    # </editor-fold>

    # <editor-fold desc="(list accessors)">
    def add_section_def(self, section: SectionDefinitionCL):
        if section is not None:
            self.__section_def_list.append(section)

    def get_section_def_list(self) -> List[SectionDefinitionCL]:
        return self.__section_def_list

    def add_member_def(self, member: AbstractMemberDefinition):
        if member is not None:
            self.__member_def_list.append(member)

    def get_member_def_list(self) -> List[AbstractMemberDefinition]:
        return self.__member_def_list
    # </editor-fold>

    #-----------------------------------
    def get_file_name_to_write_on(self):
        name = self.compound_name
        definition = Utils.get_file_name_to_write_on(name)
        return definition
    #-----------------------------------

    # <editor-fold desc="(__str__)">
    def __str__(self):
        out = ""

        if self.__kind is not None:
            out = out + "\n" + str(self.__kind)

        if self.__id is not None:
            out = out + "\n" + str(self.__id)

        if self.__compound_name is not None:
            out = out + "\n" + str(self.__compound_name)

        return out
    # </editor-fold>

    # <editor-fold desc="(__calculate_max())">
    # this function
    #   (1) adds the items in a member definition list into dictionaries
    #         (i) dictionary of attributes
    #         (ii) dictionary of functions
    #   (2) finds the longest attribute name from a member definition list
    #   (3) finds the longest function name from a member definition list
    #
    # members definition lists can be found
    #   (a) in each section definitions in a class-definitions
    #   (b) in class definitions
    def __calculate_max(self, member_definition_list: List[AbstractMemberDefinition]):
        key_index_int = 0
        for member_def_list_item in member_definition_list:
            # do not process any private or protected members
            is_include_private_bool = InMemoryElements.Is_Include_Private
            if not is_include_private_bool:
                if member_def_list_item.protection == "private" or member_def_list_item.protection == "protected":
                    continue
                # END of ... inner if
            # END of ... outer if
            kind_str = member_def_list_item.kind
            if "variable" == kind_str:
                var_key_str = member_def_list_item.get_file_name_to_write_on()  # obtain id and use it as a key
                if Utils.key_exists(self.__attr_dictionary, var_key_str): # if the key already exists in the dictionary
                    var_key_str += str(key_index_int) # attach an integer value to the key
                    key_index_int += 1 # increment the integer value for next key usage
                # END of ... inner IF
                member_def_list_item.file_name_overload = var_key_str
                self.__attr_dictionary[var_key_str] = member_def_list_item # store the member-definition in the dictionary
                # do a bubble sort to find the attribute name with max length
                attr_name_length_int = len(str(member_def_list_item.name))  # obtain the length of the name
                if self.__max_length_of_variable_name_int < attr_name_length_int:
                    self.__max_length_of_variable_name_int = attr_name_length_int
                # END of ... inner IF
                # do a bubble sort to find the attribute description with max length
                attr_desc_length_int = len(str(member_def_list_item.get_brief_description())) # obtain the length of the description
                if self.__max_length_of_variable_desc_int < attr_desc_length_int:
                    self.__max_length_of_variable_desc_int = attr_desc_length_int
                # END of ... inner IF
            elif "function" == kind_str:
                func_key_str = member_def_list_item.get_file_name_to_write_on()  # obtain id and use it as a key
                if Utils.key_exists(self.__function_dictionary, func_key_str):
                    func_key_str += str(key_index_int)
                    key_index_int += 1
                else:
                    key_index_int = 0
                # END of ... inner IF-ELSE
                member_def_list_item.file_name_overload = func_key_str # in case of overloaded functions file-name muct be suffixed with 0,1,2...
                self.__function_dictionary[func_key_str] = member_def_list_item
                self.__function_rst_dictionary[func_key_str] = member_def_list_item.get_rst()
                # do a bubble sort to find the function name with max length
                func_name_length_int = len(str(member_def_list_item.get_name_and_args_string() + "<" + func_key_str + ">" ))
                if self.__max_length_of_function_name_int < func_name_length_int:
                    self.__max_length_of_function_name_int = func_name_length_int
                # END of ... inner IF
                # do a bubble sort to find the function description with max length
                func_desc_length_int = len(str(member_def_list_item.get_brief_description()))
                if self.__max_length_of_function_desc_int < func_desc_length_int:
                    self.__max_length_of_function_desc_int = func_desc_length_int
                # END of ... inner IF
    # </editor-fold>

    # <editor-fold desc="__prepare_all_members_rst()">
    # This method generates rst for individual member functions in the class
    # so that they can be printed as individual files.
    # this method is called in the main function.
    def __prepare_all_members_rst(self):
        section_def_list = self.get_section_def_list()
        # process member definitions in each section
        member_def_list = []
        for sec in section_def_list:
            if sec is not None:
                member_def_list = member_def_list + sec.get_member_def_list()

        # To return a new list, use the sorted() built-in function...
        new_member_def_list = sorted(member_def_list, key=lambda x: x.name, reverse=False)
        # process members definitions located below the sections groups
        self.__calculate_max(new_member_def_list)
    # </editor-fold>

    # <editor-fold desc="_get_header()">
    def _get_header(self):
        top_link_name_str = self.get_file_name_to_write_on()
        text_str = ".. _" + str(top_link_name_str) + ":\n\n"
        compound_name_of_class_str = self.compound_name
        line_equal_str = "=" * len(str(compound_name_of_class_str))
        text_str += line_equal_str + "\n"
        text_str += str(compound_name_of_class_str) + "\n"
        text_str += line_equal_str + "\n\n"
        kind_name_str = self.kind
        text_str += kind_name_str
        line_caret_str = "=" * len(kind_name_str)
        text_str += "\n" + line_caret_str + "\n\n\n"
        return text_str
    # </editor-fold>

    # <editor-fold desc="_get_attributes_summary()">
    def _get_attributes_summary(self):
        text_str = ""
        # if the attribute dictionary is empty, no need to proceed
        if len(self.__attr_dictionary) < 1:
            return text_str
        # END of ... IF
        attributes_summery_str = "Attributes Summary"
        text_str += attributes_summery_str + "\n"
        line_equal_str = "=" * len(attributes_summery_str)
        text_str += line_equal_str + "\n"
        # extra 2 spaces for the gap in either sides of a variable-name
        # extra 2 spaces for back ticks around attribute names
        # extra 1 space for a dash
        var_table_row_dash_left_str = "-" * (self.__max_length_of_variable_name_int + 2 + 2 + 1)
        # extra 2 spaces for the gap in either sides of a variable-value
        var_table_row_dash_right_str = "-" * (self.__max_length_of_variable_desc_int + 2)
        text_str += "+" + var_table_row_dash_left_str + "+" + var_table_row_dash_right_str + "+" + "\n"
        gap_len_left_int = 0
        for attr_key_str in sorted(self.__attr_dictionary):
            attr_name_str = str(self.__attr_dictionary[attr_key_str].name)
            attr_desc_str = str(self.__attr_dictionary[attr_key_str].get_brief_description())
            gap_len_left_int = self.__max_length_of_variable_name_int - len(str(attr_name_str))
            gap_len_right_int = self.__max_length_of_variable_desc_int - len(str(attr_desc_str))
            text_str += "| `" + attr_name_str + "`_" + " " * gap_len_left_int + " | " + attr_desc_str + " " * gap_len_right_int + " |" + "\n"
            text_str += "+" + var_table_row_dash_left_str + "+" + var_table_row_dash_right_str + "+" + "\n"
        # END of ... for loop
        return text_str
    # </editor-fold>

    # <editor-fold desc="(_get_methods_summary())">
    def _get_methods_summary(self):
        text_str = ""

        if len(self.__function_dictionary) < 1:
            return text_str

        method_summery_str = "Method Summary"
        text_str += "\n\n" + method_summery_str + "\n"
        line_equal_str = "=" * len(method_summery_str)
        text_str += line_equal_str + "\n"
        # 2 extra spaces for the gap on either sides of a variable-name
        # 2 more spaces for backticks on either sides of a variable-name
        extra_len_int = len(":ref:") + 2 + 2
        f_table_row_dash_left_str = "-" * (self.__max_length_of_function_name_int + extra_len_int)
        f_table_row_dash_right_str = "-" * (self.__max_length_of_function_desc_int + 2)
        text_str += "+" + f_table_row_dash_left_str + "+" + f_table_row_dash_right_str + "+" + "\n"

        gap_len_left_int = 0
        for func_key_str in sorted(self.__function_dictionary):
            func_obj = self.__function_dictionary[func_key_str]
            func_name_str = func_obj.get_name_and_args_string()
            file_name_str = func_key_str
            file_extension_str = InMemoryElements.Default_File_Extension
            if InMemoryElements.Is_File_Extension:
                file_name_str = Utils.concat(file_name_str, ".", file_extension_str, ".inc")
            # END of ... if
            self._bottom_links_str += "\n.. include:: " + file_name_str
            func_desc_str = str(func_obj.get_brief_description())
            gap_len_left_int = self.__max_length_of_function_name_int - len(str(func_name_str + "<" + func_key_str + ">"))
            gap_len_right_int = self.__max_length_of_function_desc_int - len(str(func_desc_str))
            text_str += "| :ref:`" + func_name_str + "<" + func_key_str + ">`" + " " * gap_len_left_int + " | " + func_desc_str + " " * gap_len_right_int + " |" + "\n"
            text_str += "+" + f_table_row_dash_left_str + "+" + f_table_row_dash_right_str + "+" + "\n"
        return text_str
    # END of ... _get_methods_summary()
    # </editor-fold>

    # <editor-fold desc="_get_attr_doc()">
    def _get_attr_doc(self):
        text_str = ""
        # if the attribute dictionary is empty, no need to proceed
        if len(self.__attr_dictionary) < 1:
            return text_str
        # END of ... IF
        attribute_documentation = "Attribute Documentation"
        text_str += "\n" + attribute_documentation
        attribute_documentation_underline = "=" * len(attribute_documentation)
        text_str += "\n" + attribute_documentation_underline
        for key in self.__attr_dictionary:
            text_str += "\n.. _" + self.__attr_dictionary[key].name + ":"
            text_str += "\n\n`" + self.__attr_dictionary[key].name + "`"
            text_str += "\n    " + str(self.__attr_dictionary[key].get_brief_description())
            text_str += "\n\n"
        return text_str
    # </editor-fold>

    # <editor-fold desc="_get_bottom_links()">
    def _get_bottom_links(self):
        return "\n" + self._bottom_links_str
    # </editor-fold>

    # <editor-fold desc="__prepare_class_itself()">
    def __prepare_class_itself(self):
        self.__class_rst += self._get_header()
        self.__class_rst += self._get_attributes_summary()
        self.__class_rst += self._get_methods_summary()
        self.__class_rst += self._get_attr_doc()
        self.__class_rst += self._get_bottom_links()
    # </editor-fold>

    # <editor-fold desc="process()">
    # this function
    #  - generates all necessary texts and objects from a class-definition
    def process(self):
        self.__prepare_all_members_rst()
        self.__prepare_class_itself()
    # </editor-fold>

    # <editor-fold desc="(get_rst())">
    def get_rst(self):
        return self.__class_rst
    # </editor-fold>

    # <editor-fold desc="(get_individual_functions_rst())">
    def get_individual_functions_rst(self):
        return self.__function_rst_dictionary
    # </editor-fold>

def test():
    abs = AbstractCompoundDefinition()
    abs.kind = "kind"
    abs.id = "id"
    abs.compound_name = "compound_name"
    print(abs)


if __name__ == "__main__":
    test()
