from definitions.AbstractNonEnumMemberDefinition import AbstractNonEnumMemberDefinition


class TypedefMemberDefinition(AbstractNonEnumMemberDefinition):
    def __init__(self):
        super().__init__()

    def get_key_from_id(self):
        return super().get_key_from_id()

    def __str__(self):
        return super().__str__()