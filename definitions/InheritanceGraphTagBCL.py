from typing import List

from definitions.NodeTagBCL import NodeTagBCL


class InheritanceGraphTagBCL:
    __node_list: List[NodeTagBCL]

    def __init__(self):
        self.__node_list = []

    def add_node(self, node: NodeTagBCL):
        self.__node_list.append(node)

    def get_node_list(self) -> List[NodeTagBCL]:
        return self.__node_list

    # <editor-fold desc="(to string)">
    def __str__(self) -> str:
        out = "\n"

        if self.__node_list is not None:
            for sec in self.__node_list:
                if sec is not None:
                    out += "\n" + str(sec)

        return out
    # </editor-fold>
