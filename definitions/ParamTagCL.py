from typing import List

from TagTypeEnum import TagTypeEnum
from definitions.AbstractDescriptionItemBase import AbstractDescriptionItemBase


class ParamTagCL(AbstractDescriptionItemBase):
    __type_name: str
    __text_list: List[str]

    def __init__(self):
        super().__init__()
        self.__type_name = None
        self.__text_list = []

    @property
    def type_name(self) -> str:
        return self.__type_name

    @type_name.setter
    def type_name(self, type_name: str):
        self.__type_name = type_name

    def get_item_list(self) -> List[str]:
        return self.__text_list

    def add_item(self, text: str):
        self.__text_list.append(text)

    @property
    def tail(self) -> str:
        return self.__tail

    @tail.setter
    def tail(self, tail: str):
        self.__tail = tail

    def __str__(self):
        out = ""

        if self.__type_name is not None:
            out += "\n" + str(self.__type_name)

        for sss in self.__text_list:
            out += "\n" + sss

        return out

