# this class represents a <sectiondef> tag in the xml
from typing import List

from definitions.AbstractMemberDefinition import AbstractMemberDefinition
from definitions.FunctionMemberDefinitionCL import FunctionMemberDefinitionCL
from definitions.LocationDefinitionCL import LocationDefinitionCL
from definitions.MemberTagCL import MemberTagCL
from definitions.ParamTagCL import ParamTagCL
from definitions.VariableMemberDefinitionCL import VariableMemberDefinitionCL


class SectionDefinitionCL:
    __kind: str
    __member_def_list: List[AbstractMemberDefinition]

    def __init__(self):
        self.__kind = None
        self.__member_def_list = []

    @property
    def kind(self) -> str:
        return self.__kind

    @kind.setter
    def kind(self, kind: str):
        self.__kind = kind

    # add a member object to the list
    def add_member(self, member_def: AbstractMemberDefinition):
        if member_def is not None:
            self.__member_def_list.append(member_def)

    # return the section list
    def get_member_def_list(self) -> List[AbstractMemberDefinition]:
        return self.__member_def_list

    def __str__(self) -> str:
        out = str(self.__kind)

        if self.__member_def_list is not None:
            for member in self.__member_def_list:
                if member is not None:
                    out += "\n" + str(member)

        return out

    def get_rst(self):
        rst = ".. _"

        for m in self.__member_def_list:
            rst = rst + m.get_rst()

        return rst

