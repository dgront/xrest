from typing import List

from TagTypeEnum import TagTypeEnum
from definitions.AbstractDescriptionItemBase import AbstractDescriptionItemBase


class ComputerOutputTag(AbstractDescriptionItemBase):
    def __init(self):
        self._tag_type = TagTypeEnum.ComputerOutput
        self.__text_list: List[str] = []

    def get_item_list(self):
        return self.__text_list

    def add_item(self, t):
        self.__text_list.append(t)