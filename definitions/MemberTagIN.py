class MemberTagIN:
    __reference_id: str
    __kind: str
    __name: str

    def __init__(self):
        self.__reference_id = None
        self.__kind = None
        self.__name = None

    @property
    def reference_id(self) -> str:
        return self.__reference_id

    @reference_id.setter
    def reference_id(self, reference_id: str):
        self.__reference_id = reference_id

    @property
    def kind(self) -> str:
        return self.__kind

    @kind.setter
    def kind(self, kind: str):
        self.__kind = kind

    @property
    def name(self) -> str:
        return self.__name

    @name.setter
    def name(self, name: str):
        self.__name = name

    def __str__(self) -> str:
        out = ""

        if self.__reference_id is not None:
            out += "\n" + str(self.__reference_id)

        if self.__kind is not None:
            out += "\n" + str(self.__kind)

        if self.__name is not None:
            out += "\n" + str(self.__name)

        return out
