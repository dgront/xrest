from definitions.CompoundRefTagBCL import CompoundRefTagBCL


class DerivedCompoundRefTagBCL(CompoundRefTagBCL):
    def __init__(self):
        super().__init__()

    def __str__(self):
        return "\n" + super().__str__()