from definitions.PlainText import PlainText


class ParameterNameTagCL(PlainText):
    def __init__(self):
        super().__init__()

    def __str__(self) -> str:
        out = super().__str__()
        return out

    @classmethod
    def get_test_object(cls):
        para_desc_tag_cl = ParameterNameTagCL()
        para_desc_tag_cl.text = "hello 222"
        return para_desc_tag_cl

if __name__ == "__main__":
    ooo = ParameterNameTagCL.get_test_object()
    print(ooo)