# this class represents a <memberdef> node in the xml file.
from typing import Optional

from Utils import Utils
from definitions.AbstractMemberDefinition import AbstractMemberDefinition
from abc import ABC, abstractmethod


class AbstractNonEnumMemberDefinition(AbstractMemberDefinition):
    # <editor-fold desc="(constructor)">
    def __init__(self):
        super().__init__()
        self.__data_type: str = None
        self.__definition: str = None
        self.__args_string: str = None
    # </editor-fold>

    # <editor-fold desc="(basic type members)">
    @property
    def data_type(self) -> str:
        return self.__data_type

    @data_type.setter
    def data_type(self, data_type: str):
        self.__data_type = data_type

    @property
    def definition(self) -> str:
        return self.__definition

    @definition.setter
    def definition(self, definition: str):
        self.__definition = definition

    @property
    def args_string(self) -> str:
        return self.__args_string

    @args_string.setter
    def args_string(self, arg_string: str):
        self.__args_string = arg_string

    # </editor-fold>

    def get_file_name_to_write_on(self):
        name = self.definition
        char_list = [' ', ':', '<', '>', '(', ')', '&', '~', '*', ',', '.', '/', '\\', '[', ']', '=', '+', '-']
        definition = Utils.replace_chars(name, char_list, "_")
        super_name = super().get_file_name_to_write_on()
        return super_name + "_" + definition

    # <editor-fold desc="(to string)">
    def __str__(self):
        out = super().__str__()

        if self.__data_type is not None:
            out = out + "\n" + str(self.__data_type)

        if self.__definition is not None:
            out = out + "\n" + str(self.__definition)

        if self.__args_string is not None:
            out = out + "\n" + str(self.__args_string)

        return out

    # </editor-fold>

    def get_rst(self):
        rst = ""
        if self.__data_type is None:
            rst += super().get_rst()
            rst += self.args_string
        else:
            rst += str(self.data_type) + " " + super().get_rst() + self.args_string

        return rst




