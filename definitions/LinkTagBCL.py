class LinkTagBCL:
    __reference_id: str

    def __init__(self):
        self.__reference_id = None

    @property
    def reference_id(self) -> str:
        return self.__reference_id

    @reference_id.setter
    def reference_id(self, reference_id: str):
        self.__reference_id = reference_id

    def printf(self):
        print(self.__reference_id)

    # <editor-fold desc="(to string)">
    def __str__(self) -> str:
        out = ""

        if self.__reference_id is not None:
            out += "\n" + str(self.__reference_id)

        return out
    # </editor-fold>