from definitions.ParameterListTagCL import ParameterListTagCL

class BaseDescription:
    # <editor-fold desc="(constructor)">
    def __init__(self):
        self.__text: str = None
        self.__parameter_list : ParameterListTagCL = None
    # </editor-fold>

    @property
    def text(self):
        return self.__text

    @text.setter
    def text(self, t):
        self.__text = t

    # <editor-fold desc="(list accessors)">
    def set_parameter_list(self, para_deeply_nested: ParameterListTagCL):
        self.__parameter_list = para_deeply_nested

    def get_parameter_list(self) -> ParameterListTagCL:
        return self.__parameter_list
    # </editor-fold>

    def get_description(self, parameter_name_str:str):
        description_str = ""
        if self.__parameter_list is not None:
            parameter_item_list = self.__parameter_list.get_item_list()
            for item in parameter_item_list:
                name_str = item.get_parameter_name_list().get_parameter_name().text
                desc_str = item.get_parameter_description().text
                if name_str == parameter_name_str:
                    description_str += desc_str
                    break
                # END of IF
            # END of FOR loop
        return description_str
    # END of ... get_description()

    def __str__(self):
        if self.text is not None:
            out = self.text
        else:
            out = ""
        # END if

        if self.__parameter_list is not None:
            out+=str(self.__parameter_list)
        # END of ... if
        return out
    # END of ... __str__()

    @classmethod
    def get_test_object(cls):
        base_desc_obj = BaseDescription()

        sst_item1 = ParaTag.get_test_object()
        sst_item2 = ParaTag.get_test_object()

        base_desc_obj.add_deep_para(sst_item1)
        base_desc_obj.add_deep_para(sst_item2)

        return base_desc_obj


if __name__ == "__main__":
    ooo = BaseDescription.get_test_object()
    print(ooo)