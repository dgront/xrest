from definitions.BaseDescription import BaseDescription


class BriefDescription(BaseDescription):
    def __init__(self):
        super().__init__()

    def __str__(self):
        out = super().__str__()

        return out

    @classmethod
    def get_test_object1(cls):
        base_desc_obj = BriefDescription()

        plt_item1 = ParaTextOnlyTagCL.get_test_object()
        plt_item2 = ParaTextOnlyTagCL.get_test_object()

        sst_item1 = ParaDeeplyNestedTagCL.get_test_object()
        sst_item2 = ParaDeeplyNestedTagCL.get_test_object()

        base_desc_obj.add_simple_para(plt_item1)
        base_desc_obj.add_simple_para(plt_item2)

        base_desc_obj.add_deep_para(sst_item1)
        base_desc_obj.add_deep_para(sst_item2)

        return base_desc_obj

    @classmethod
    def get_test_object2(cls):
        brief_desc_obj = BriefDescription()

        brief_desc_obj = BaseDescription.get_test_object()

        return brief_desc_obj


if __name__ == "__main__":
    ooo = BriefDescription.get_test_object2()
    print(ooo)
