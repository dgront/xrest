from typing import List, Dict

from ClassStructDictElement import ClassStructDictElement
from FileIO import FileIO
from SystemUtils import SystemUtils
from Utils import BColors, Utils
from definitions.CompoundTagIN import CompoundTagIN
from definitions.NamespaceCompoundDefinition import NamespaceCompoundDefinition


class IndexIN:
    __compound_list: List[CompoundTagIN]

    def __init__(self):
        self.__compound_list = []

    def get_compound_list(self) -> List[CompoundTagIN]:
        return self.__compound_list

    def add_compound(self, compound: CompoundTagIN):
        self.__compound_list.append(compound)

    def __str__(self) -> str:
        out = ""

        if self.__compound_list is not None:
            for sec in self.__compound_list:
                if sec is not None:
                    out += "\n" + str(sec)

        return out

    # def get_rst(self):
    #     rst = ""
    #     title_text = "Welcome to XXXXX's documentation!"
    #     underline = "=" * len(title_text)
    #
    #     rst += title_text + "\n" + underline
    #
    #     rst += "\n\n" + ".. toctree::"
    #     rst += "\n" + "   :maxdepth: 1"
    #     rst += "\n" + "   :caption: Documentation"
    #     rst += "\n" + "   :name: sec-doc"
    #
    #     for comp in self.__compound_list:
    #         key_str = comp.get_file_name_to_write_on()
    #         path_str = self.__index_dictionary[key_str]
    #         rst += "\n" + path_str + "\n" + comp.reference_id
    #
    #     return rst

    def get_main_index_rst(self, index_list: List[str], file_extension_str):
        rst = ""

        if len(index_list) < 1:
            return

        header_link = ".. _index:"
        rst += header_link + "\n\n"

        # don't modify the indentation of following text between triple quotes.
        # the exact indentation is crucial for the output text.
        header_str = """.. shapes documentation master file, created by
   sphinx-quickstart on Wed May  5 09:01:16 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to BioShell documentation!
=================================="""

        rst += header_str + "\n\n"

        toctree_str = ".. toctree::\n"
        toctree_str += "   :maxdepth: 2\n"
        toctree_str += "   :caption: Contents:"

        rst += toctree_str + "\n\n"

        sorted_index_list = sorted(index_list)

        for item in sorted_index_list:
            rst += "   src/" + item + "\n"
            # END of ... if-else
        # END of ... for
        rst += "\n\n"

        bottom_test_str = """Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`"""

        rst += bottom_test_str
        return rst

    # def get_main_index_rst(self, namespace_dictionary: Dict[str, NamespaceCompoundDefinition], file_extension_str):
    #     rst = ""
    #
    #     if len(namespace_dictionary) < 1:
    #         return
    #
    #     header_link = ".. _index"
    #     rst += header_link + "\n\n"
    #
    #     toctree_str =  ".. toctree::\n"
    #     toctree_str += "   :maxdepth: 2\n"
    #     toctree_str += "   :caption: Contents:"
    #
    #     rst += toctree_str + "\n\n"
    #
    #     for key_str in namespace_dictionary:
    #         namespace_obj = namespace_dictionary[key_str]
    #         namespace_dir_str = namespace_obj.get_dir()
    #         namespace_file_str = namespace_obj.get_file_name_to_write_on()
    #         rst += "   " + SystemUtils.merge_paths(namespace_dir_str, namespace_file_str) + "\n"
    #         # END of ... if-else
    #     # END of ... for
    #     rst += "\n\n"
    #
    #     return rst
