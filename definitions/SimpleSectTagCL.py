from typing import List

from definitions.AbstractDescriptionItemBase import AbstractDescriptionItemBase
from TagTypeEnum import TagTypeEnum
from definitions.BoldTag import BoldTag
from definitions.ComputerOutputTag import ComputerOutputTag
from definitions.FormulaTag import FormulaTag
from definitions.RefParamTagCL import RefParamTagCL


class SimpleSectTagCL:
    __kind: str
    __items_list: List[AbstractDescriptionItemBase]

    def __init__(self):
        super().__init__()
        self.__kind = None
        self.__items_list = []

    @property
    def kind(self) -> str:
        return self.__kind

    @kind.setter
    def kind(self, kind: str):
        self.__kind = kind

    def get_items_list(self) -> List[AbstractDescriptionItemBase]:
        return self.__items_list

    def add_item(self, item: AbstractDescriptionItemBase):
        self.__items_list.append(item)

    # <editor-fold desc="(to string)">
    def __str__(self) -> str:
        out = "\n"

        if self.__kind is not None:
            out += "\n" + str(self.__kind)

        for sss in self.__formula_list:
            out += "\n" + str(sss)

        return out
    # </editor-fold>

    @classmethod
    def get_test_object(cls):
        s = SimpleSectTagCL()
        s.kind = ":::kindness:::"
        s.para = ":::parameter:::"
        return s


if __name__ == "__main__":
    print(SimpleSectTagCL.get_test_object())