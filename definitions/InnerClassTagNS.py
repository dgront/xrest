from Utils import Utils


class InnerClassTagNS:
    __reference_id: str
    __protection: str
    __text: str
    #__location: str

    def __init__(self):
        self.__reference_id = None
        self.__protection = None
        self.__text = None
        #self.__location = None

    @property
    def reference_id(self) -> str:
        return self.__reference_id

    @reference_id.setter
    def reference_id(self, reference_id: str):
        self.__reference_id = reference_id

    @property
    def protection(self) -> str:
        return self.__protection

    @protection.setter
    def protection(self, protection: str):
        self.__protection = protection

    @property
    def text(self) -> str:
        return self.__text

    @text.setter
    def text(self, text: str):
        self.__text = text

    # @property
    # def location(self) -> str:
    #     return self.__location
    #
    # @location.setter
    # def location(self, location: str):
    #     self.__location = location

    def get_file_name_to_write_on(self):
        return Utils.get_file_name_to_write_on(self.text)

    def __str__(self) -> str:
        out = "\n"

        if self.__reference_id is not None:
            out += "\n" + str(self.__reference_id)

        if self.__protection is not None:
            out += "\n" + str(self.__protection)

        if self.__text is not None:
            out += "\n" + str(self.__text)

        return out
