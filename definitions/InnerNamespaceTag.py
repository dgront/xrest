
class InnerNamespaceTag:
    __reference_id: str
    __text: str

    def __init__(self):
        self.__reference_id = None
        self.__text = None

    @property
    def reference_id(self) -> str:
        return self.__reference_id

    @reference_id.setter
    def reference_id(self, reference_id: str):
        self.__reference_id = reference_id

    @property
    def text(self) -> str:
        return self.__text

    @text.setter
    def text(self, text: str):
        self.__text = text

    # <editor-fold desc="(to string)">
    def __str__(self) -> str:
        out = ""

        if self.__reference_id is not None:
            out += "\n" + str(self.__reference_id)

        if self.__text is not None:
            out += "\n" + str(self.__text)

        return out
    # </editor-fold>