from typing import Optional

from definitions.ParameterNameTagCL import ParameterNameTagCL


class ParameterNameListTagCL:
    __parameter_name: ParameterNameTagCL

    def __init__(self):
        self.__parameter_name = None

    def get_parameter_name(self) -> ParameterNameTagCL:
        return self.__parameter_name

    def set_parameter_name(self, parameter_name: ParameterNameTagCL):
        self.__parameter_name = parameter_name

    # <editor-fold desc="(to string)">
    def __str__(self) -> str:
        out = ""

        if self.__parameter_name is not None:
            out += "\n" + str(self.__parameter_name)

        return out

    # </editor-fold>

    @classmethod
    def get_test_object(cls):
        pntcl_obj = ParameterNameTagCL()
        pntcl_obj.text = ":::parameter name tag from class:::"

        pnlt_obj = ParameterNameListTagCL()
        pnlt_obj.set_parameter_name(pntcl_obj)

        return pnlt_obj


if __name__ == "__main__":
    ooo = ParameterNameListTagCL.get_test_object()
    print(ooo)
