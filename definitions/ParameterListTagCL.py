from typing import List

from definitions.ParameterItemTagCL import ParameterItemTagCL


class ParameterListTagCL:
    __kind: str
    __parameter_item_list: List[ParameterItemTagCL]

    def __init__(self):
        super().__init__()
        self.__kind = None
        self.__parameter_item_tag_cl = []

    @property
    def kind(self) -> str:
        return self.__kind

    @kind.setter
    def kind(self, kind: str):
        self.__kind = kind

    def add_item(self, parameter_item: ParameterItemTagCL):
        self.__parameter_item_tag_cl.append(parameter_item)

    def get_item_list(self) -> List[ParameterItemTagCL]:
        return self.__parameter_item_tag_cl

    # <editor-fold desc="(to string)">
    def __str__(self) -> str:
        out = ""

        if self.__kind is not None:
            out += str(self.__kind)

        out += "\n"

        if self.__parameter_item_tag_cl is not None:
            for sec in self.__parameter_item_tag_cl:
                if sec is not None:
                    out += str(sec) + ", "

        return out
    # </editor-fold>

    @classmethod
    def get_test_object(cls):
        plt_obj = ParameterListTagCL()
        plt_obj.kind = ":::kind:::"

        item1 = ParameterItemTagCL.get_test_object()
        item2 = ParameterItemTagCL.get_test_object()

        plt_obj.add_item(item1)
        plt_obj.add_item(item2)

        return plt_obj


if __name__ == "__main__":
    ooo = ParameterListTagCL.get_test_object()
    print(ooo)