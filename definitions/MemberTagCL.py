class MemberTagCL:
    __reference_id: str
    __protection: str
    __virtual: str
    __scope: str
    __name: str

    def __init__(self):
        self.__reference_id = None
        self.__protection = None
        self.__virtual = None
        self.__scope = None
        self.__name = None

    @property
    def reference_id(self) -> str:
        return self.__reference_id

    @reference_id.setter
    def reference_id(self, reference_id: str):
        self.__reference_id = reference_id

    @property
    def protection(self) -> str:
        return self.__protection

    @protection.setter
    def protection(self, protection: str):
        self.__protection = protection

    @property
    def virtual(self) -> str:
        return self.__virtual

    @virtual.setter
    def virtual(self, virtual: str):
        self.__virtual = virtual

    @property
    def scope(self) -> str:
        return self.__scope

    @scope.setter
    def scope(self, scope: str):
        self.__scope = scope

    @property
    def name(self) -> str:
        return self.__name

    @name.setter
    def name(self, name: str):
        self.__name = name

    def __str__(self) -> str:
        out = ""

        if self.__reference_id is not None:
            out += "\n" + str(self.__reference_id)

        if self.__protection is not None:
            out += "\n" + str(self.__protection)

        if self.__virtual is not None:
            out += "\n" + str(self.__virtual)

        if self.__scope is not None:
            out += "\n" + str(self.__scope)

        if self.__name is not None:
            out += "\n" + str(self.__name)

        return out

