
class EnumValueTag:
    def __init__(self):
        self.__id: str = None
        self.__protection: str = None
        self.__name: str = None
        self.__brief_description: str = None
        self.__detailed_description: str = None

    @property
    def id(self):
        return self.__id

    @id.setter
    def id(self, id):
        self.__id = id

    @property
    def protection(self):
        return self.__protection

    @protection.setter
    def protection(self, protection):
        self.__protection = protection

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, name):
        self.__name = name

    @property
    def brief_description(self):
        return self.__brief_description

    @brief_description.setter
    def brief_description(self, brief_description):
        self.__brief_description = brief_description

    @property
    def detailed_description(self):
        return self.__detailed_description

    @detailed_description.setter
    def detailed_description(self, detailed_description):
        self.__detailed_description = detailed_description

    # <editor-fold desc="(to string)">
    def __str__(self) -> str:
        out = "\n"

        if self.__id is not None:
            out += "\n" + str(self.__id)

        if self.__protection is not None:
            out += "\n" + str(self.__protection)

        if self.__name is not None:
            out += "\n" + str(self.__name)

        if self.__brief_description is not None:
            out += "\n" + str(self.__brief_description)

        if self.__detailed_description is not None:
            out += "\n" + str(self.__detailed_description)

        return out
    # </editor-fold>

