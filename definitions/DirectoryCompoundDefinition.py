from typing import List, Optional

from definitions.AbstractCompoundDefinition import AbstractCompoundDefinition
from definitions.BriefDescription import BriefDescription
from definitions.DetailedDescription import DetailedDescription
from definitions.InnerFileDefinitionDR import InnerFileDefinitionDR
from definitions.LocationDefinitionCL import LocationDefinitionCL


class DirectoryCompoundDefinition(AbstractCompoundDefinition):
    def __init__(self):
        super().__init__()
        self.__inner_file_list: List[InnerFileDefinitionDR] = []
        self.__brief_description: BriefDescription = None
        self.__detailed_description: DetailedDescription = None
        self.__location: LocationDefinitionCL = None

    @property
    def brief_description(self) -> BriefDescription:
        return self.__brief_description

    @brief_description.setter
    def brief_description(self, brief_description:BriefDescription):
        self.__brief_description = brief_description

    @property
    def detailed_description(self) -> DetailedDescription:
        return self.__detailed_description

    @detailed_description.setter
    def detailed_description(self, detailed_description: DetailedDescription):
        self.__detailed_description = detailed_description

    # add an inner-class item to the list
    def add_inner_file(self, inner_file: InnerFileDefinitionDR):
        if inner_file is not None:
            self.__inner_file_list.append(inner_file)

    def get_inner_file(self) -> List[InnerFileDefinitionDR]:
        return self.__inner_file_list

    @property
    def location(self) -> LocationDefinitionCL:
        return self.__location

    @location.setter
    def location(self, location: LocationDefinitionCL):
        self.__location = location

    def __str__(self) -> str:
        out = str(super())  # print abstract class attributes

        if self.__detailed_description is not None:
            out += "\n" + str(self.__detailed_description)

        if self.__brief_description is not None:
            out += "\n" + str(self.__brief_description)

        if self.__location is not None:
            out += "\n" + str(self.__location)

        # if the list is not-null, print its elements
        if self.__inner_file_list is not None:
            for icl in self.__inner_file_list:
                # if the individual element is not-null, print its content
                if icl is not None:
                    out += "\n" + str(icl)

        return out
