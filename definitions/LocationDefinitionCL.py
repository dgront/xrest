# this class represents <location> tag in the xml
from FileIO import FileIO


class LocationDefinitionCL:
    __file: str
    __line: str
    __column: str
    __body_file: str
    __body_start: str
    __body_end: str

    def __init__(self):
        self.__file = None
        self.__line = None
        self.__column = None
        self.__body_file = None
        self.__body_start = None
        self.__body_end = None

    @property
    def file(self) -> str:
        return self.__file

    @file.setter
    def file(self, file: str):
        self.__file = file

    @property
    def line(self) -> str:
        return self.__line

    @line.setter
    def line(self, line: str):
        self.__line = line

    @property
    def column(self) -> str:
        return self.__column

    @column.setter
    def column(self, column: str):
        self.__column = column

    @property
    def body_file(self) -> str:
        return self.__body_file

    @body_file.setter
    def body_file(self, body_file: str):
        self.__body_file = body_file

    @property
    def body_start(self) -> str:
        return self.__body_start

    @body_start.setter
    def body_start(self, body_start: str):
        self.__body_start = body_start

    @property
    def body_end(self) -> str:
        return self.__body_end

    @body_end.setter
    def body_end(self, body_end: str):
        self.__body_end = body_end

    def get_path_and_file_name(self):
        return FileIO.parse_path_and_file_name(self.file)

    def __str__(self) -> str:
        out = ""

        if self.__file is not None:
            out += "\n" + str(self.__file)

        if self.__line is not None:
            out += "\n" + str(self.__line)

        if self.__column is not None:
            out += "\n" + str(self.__column)

        if self.__body_file is not None:
            out += "\n" + str(self.__body_file)

        if self.__body_start is not None:
            out += "\n" + str(self.__body_start)

        if self.__body_end is not None:
            out += "\n" + str(self.__body_end)

        return out
