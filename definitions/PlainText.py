from TagTypeEnum import TagTypeEnum
from definitions.AbstractDescriptionItemBase import AbstractDescriptionItemBase


class PlainText(AbstractDescriptionItemBase):
    __text: str

    def __init__(self):
        super().__init__()
        self.__text = ""

    @property
    def text(self):
        return self.__text

    @text.setter
    def text(self, ttt):
        self.__text = ttt

    def __str__(self) -> str:
        #out = super().__str__()

        out = "\n" + str(self.__text)

        return out

    @classmethod
    def get_test_object(cls):
        plain_text_obj = PlainText()
        plain_text_obj.text = "hello x-y-z hello"
        return plain_text_obj


if __name__ == "__main__":
    ooo = PlainText.get_test_object()
    print(ooo)