class IncludesTagBCL:
    __reference_id: str
    __local: str
    __name: str

    def __init__(self):
        self.__reference_id = None
        self.__local = None
        self.__name = None

    @property
    def reference_id(self) -> str:
        return self.__reference_id

    @reference_id.setter
    def reference_id(self, reference_id: str):
        self.__reference_id = reference_id

    @property
    def local(self) -> str:
        return self.__local

    @local.setter
    def local(self, local: str):
        self.__local = local

    @property
    def name(self) -> str:
        return self.__name

    @name.setter
    def name(self, name: str):
        self.__name = name

    # <editor-fold desc="(to string)">
    def __str__(self) -> str:
        out = "\n"

        if self.__reference_id is not None:
            out += "\n" + str(self.__reference_id)

        if self.__local is not None:
            out += "\n" + str(self.__local)

        if self.__name is not None:
            out += "\n" + str(self.__name)

        return out
    # </editor-fold>
