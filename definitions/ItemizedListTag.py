from typing import List

from definitions.AbstractDescriptionItemBase import AbstractDescriptionItemBase


class ItemizedListTag(AbstractDescriptionItemBase):
    __items_list: List[AbstractDescriptionItemBase]

    def __init__(self):
        self.__items_list = []