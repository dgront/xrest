

class CompoundRefTagBCL:
    # <editor-fold desc="(constructor)">
    def __init__(self):
        self.__reference_id: str = None
        self.__protection: str = None
        self.__virtual: str = None
        self.__text: str = None
    # </editor-fold>

    # <editor-fold desc="(properties)">
    @property
    def reference_id(self)-> str:
        return self.__reference_id

    @reference_id.setter
    def reference_id(self, reference_id: str):
        self.__reference_id = reference_id

    @property
    def protection(self)-> str:
        return self.__protection

    @protection.setter
    def protection(self, protection: str):
        self.__protection = protection

    @property
    def virtual(self)-> str:
        return self.__virtual

    @virtual.setter
    def virtual(self, virtual: str):
        self.__virtual = virtual

    @property
    def name(self) -> str:
        return self.__text

    @name.setter
    def name(self, text: str):
        self.__text = text
    # </editor-fold>

    # <editor-fold desc="(to string)">
    def __str__(self) -> str:
        out = ""

        if self.__reference_id is not None:
            out += "\n" + str(self.__reference_id)

        if self.__protection is not None:
            out += "\n" + str(self.__protection)

        if self.__virtual is not None:
            out += "\n" + str(self.__virtual)

        if self.__text is not None:
            out += "\n" + str(self.__text)

        return out
    # </editor-fold>