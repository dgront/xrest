from typing import List

from definitions.MemberTagIN import MemberTagIN


class CompoundTagIN:
    # <editor-fold desc="(constructor)">
    def __init__(self):
        self.__reference_id: str = None
        self.__kind: str = None
        self.__member_tag_list: List[MemberTagIN] = []

    # </editor-fold>

    # <editor-fold desc="(properties)">
    @property
    def reference_id(self) -> str:
        return self.__reference_id

    @reference_id.setter
    def reference_id(self, reference_id: str):
        self.__reference_id = reference_id

    def get_key_from_id(self):
        return self.reference_id.replace("/", "_")

    @property
    def kind(self) -> str:
        return self.__kind

    @kind.setter
    def kind(self, kind: str):
        self.__kind = kind

    # </editor-fold>

    # <editor-fold desc="(list accessor)">
    def get_member_list(self) -> List[MemberTagIN]:
        return self.__member_tag_list

    def add_member(self, member: MemberTagIN):
        self.__member_tag_list.append(member)

    # </editor-fold>

    # <editor-fold desc="(to string)">
    def __str__(self) -> str:
        out = ""

        if self.__reference_id is not None:
            out += "\n" + str(self.__reference_id)

        if self.__kind is not None:
            out += "\n" + str(self.__kind)

        if self.__member_tag_list is not None:
            for mem in self.__member_tag_list:
                if mem is not None:
                    out += "\n" + str(mem)
        return out
    # </editor-fold>
