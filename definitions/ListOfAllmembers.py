from typing import List

from definitions.MemberTagCL import MemberTagCL


class ListOfAllmembers:
    __member_tag_list: List[MemberTagCL]

    # <editor-fold desc="(constructor)">
    def __init__(self):
        self.__member_tag_list = []

    # </editor-fold>

    # <editor-fold desc="(list accessor)">
    def add_member(self, member: MemberTagCL):
        if member is not None:
            self.__member_tag_list.append(member)

    def get_member_tag_list(self) -> List[MemberTagCL]:
        return self.__member_tag_list
    # </editor-fold>

    # <editor-fold desc="(to string)">
    def __str__(self) -> str:
        out = ""

        if self.__member_tag_list is not None:
            for param in self.__member_tag_list:
                if param is not None:
                    out += "\n" + str(param)

        return out
    # </editor-fold>

