from typing import Optional, List, Dict

from ClassStructDictElement import ClassStructDictElement
from InMemoryElements import InMemoryElements
from Utils import BColors, FileExtension, Utils
from definitions.AbstractCompoundDefinition import AbstractCompoundDefinition
from definitions.InnerClassTagNS import InnerClassTagNS


class NamespaceCompoundDefinition(AbstractCompoundDefinition):
    __all_classes_dictionary: Dict[str, ClassStructDictElement] = {}
    __file_extension_str: str = FileExtension.RST

    # <editor-fold desc="(constructor)">
    def __init__(self):
        super().__init__()
        self.__all_inner_class_list: List[InnerClassTagNS] = []
        self.__filtered_class_list: List[ClassStructDictElement] = []
        self.__max_length_of_class_name_int = 0
        self.__max_length_of_class_desc_int = 0
        self.__namespace_rst_str = ""
    # </editor-fold>

    # <editor-fold desc="(inner classes)">
    # add an inner-class item to the list
    def add_inner_class(self, inner_class: InnerClassTagNS):
        if inner_class is not None:
            self.__all_inner_class_list.append(inner_class)

    def get_inner_class_list(self) -> List[InnerClassTagNS]:
        return self.__all_inner_class_list
    # </editor-fold>

    # <editor-fold desc="(directory & file)">
    def get_dir(self) -> str:
        return super().get_dir()

    def get_file_name_to_write_on(self):
        return "namespace_" + super().get_file_name_to_write_on()
    # </editor-fold>

    @staticmethod
    def set_class_dictionary(class_dict):
        NamespaceCompoundDefinition.__all_classes_dictionary = class_dict

    @staticmethod
    def set_file_extension(file_extension):
        NamespaceCompoundDefinition.__file_extension = file_extension

    # <editor-fold desc="(__str__())">
    def __str__(self) -> str:
        out = super().__str__()  # print abstract class attributes

        if self.__all_inner_class_list is not None:
            for icl in self.__all_inner_class_list:
                if icl is not None:
                    out += "\n" + str(icl)
        return out
    # </editor-fold>

    # <editor-fold desc="__prepare_namespace_members()">
    def __prepare_namespace_members(self):
        # iterate through class-members in the namespace
        for inner_class_item in self.__all_inner_class_list:
            # do not process any private or protected members
            is_include_private_bool = InMemoryElements.Is_Include_Private
            if not is_include_private_bool:
                if inner_class_item.protection == "private" or inner_class_item.protection == "protected":
                    continue
                # END of ... inner IF
            # END of outer ... IF
            class_name_as_key_str = "class_" + inner_class_item.get_file_name_to_write_on()
            if class_name_as_key_str in NamespaceCompoundDefinition.__all_classes_dictionary:
                class_item_obj = NamespaceCompoundDefinition.__all_classes_dictionary[class_name_as_key_str]
                if class_item_obj is not None:
                    self.__filtered_class_list.append(class_item_obj) # save the class object for rst generation
                    name_len_int = len(str(class_item_obj.class_name) + "<" + class_name_as_key_str + ">") # obtain the length of the class-name
                    # select max length...
                    desc_len_int = len(str(class_item_obj.brief_description)) # obtain the length of class-description
                    if self.__max_length_of_class_name_int < name_len_int:
                        self.__max_length_of_class_name_int = name_len_int
                    # END of ... IF
                    if self.__max_length_of_class_desc_int < desc_len_int:
                        self.__max_length_of_class_desc_int = desc_len_int
                    # END of ... IF
                # END of ... IF
            else:
                error_str = "XREST Error: The class [" + class_name_as_key_str + "] not found."
                print(BColors.FAIL + error_str + BColors.ENDC)
            # END of ... if-else
        # END of ... for
    # </editor-fold>

    # <editor-fold desc="__prepare_class_summary()">
    def __prepare_class_summary(self):
        text_str = ""
        class_summery_str = "Class Summary"
        text_str += "\n\n" + class_summery_str + "\n"
        line_equal_str = "=" * len(class_summery_str)
        text_str += line_equal_str + "\n"
        # 2 extra spaces for the gap on either sides of a variable-name
        # 2 more spaces for backticks on either sides of a variable-name
        extra_len_int = len(":ref:") + 2 + 2
        table_row_dash_left_str = "-" * (self.__max_length_of_class_name_int + extra_len_int)
        table_row_dash_right_str = "-" * (self.__max_length_of_class_desc_int + 2)
        text_str += "+" + table_row_dash_left_str + "+" + table_row_dash_right_str + "+" + "\n"
        gap_len_left_int = 0
        for cls_obj_item in self.__filtered_class_list:
            cls_name_str = cls_obj_item.class_name
            cls_desc_str = cls_obj_item.brief_description
            file_name_str = cls_obj_item.unique_file_name
            file_extension_str = InMemoryElements.Default_File_Extension
            if InMemoryElements.Is_File_Extension:
                file_name_str = Utils.concat(file_name_str, ".", file_extension_str, ".inc")
            # END of ... if
            self._bottom_links_str += "\n.. include:: " + file_name_str
            gap_len_left_int = self.__max_length_of_class_name_int - len(str(cls_name_str) + "<" + cls_obj_item.unique_file_name + ">")
            gap_len_right_int = self.__max_length_of_class_desc_int - len(str(cls_desc_str))
            text_str += "| :ref:`" + cls_name_str + "<" + cls_obj_item.unique_file_name + ">`" + " " * gap_len_left_int + " | " + cls_desc_str + " " * gap_len_right_int + " |" + "\n"
            text_str += "+" + table_row_dash_left_str + "+" + table_row_dash_right_str + "+" + "\n"
        # END of ... for
        return text_str + "\n\n"
    # </editor-fold>

    # <editor-fold desc="__prepare_namespace_itself()">
    def __prepare_namespace_itself(self):
        self.__namespace_rst_str = self._get_header()
        self.__namespace_rst_str += self.__prepare_class_summary()
        self.__namespace_rst_str += self._get_attributes_summary()
        self.__namespace_rst_str += self._get_methods_summary()
        self.__namespace_rst_str += self._get_attr_doc()
        self.__namespace_rst_str += self._get_bottom_links()
    # </editor-fold>

    # <editor-fold desc="(process())">
    def process(self):
        self.__prepare_namespace_members()
        self.__prepare_namespace_itself()
    # </editor-fold>

    def get_rst(self):
        return self.__namespace_rst_str

    def get_local_index_rst(self):
        rst = ""

        header_link = ".. _index_" + self.get_file_name_to_write_on() + ":"
        rst += header_link + "\n\n"

        toctree_str = ".. toctree::\n"
        toctree_str += "   :maxdepth: 2\n"
        toctree_str += "   :caption: Contents\n\n"

        rst += toctree_str

        rst += "   "+ self.get_file_name_to_write_on() + "\n"

        # for inner_class_obj in self.__all_inner_class_list:
        #     class_name_as_key = "class_" + inner_class_obj.get_file_name_to_write_on()
        #     if class_name_as_key in self.__all_classes_dictionary:
        #         class_item_obj = self.__all_classes_dictionary[class_name_as_key]
        #         if class_item_obj is not None:
        #             rst += "   " + class_item_obj.unique_file_name + "\n"
        #     else:
        #         error_str = "XREST Error: The class [" + class_name_as_key + "] not found."
        #         print(BColors.FAIL + error_str + BColors.ENDC)
        #     # END of ... if-else
        # # END of ... for
        rst += "\n\n"
        return rst




