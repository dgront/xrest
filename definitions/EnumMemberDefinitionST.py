from typing import List

from definitions.EnumValueTag import EnumValueTag
from definitions.AbstractMemberDefinition import AbstractMemberDefinition


class EnumMemberDefinitionST(AbstractMemberDefinition):
    def __init__(self):
        super().__init__()
        self.__enumvalues_list: List[EnumValueTag] = []

    def add_enumvalue_def(self, enumvalue: EnumValueTag):
        if enumvalue is not None:
            self.__enumvalues_list.append(enumvalue)

    def get_enumvalue_def_list(self) -> List[EnumValueTag]:
        return self.__enumvalues_list

    def get_key_from_id(self):
        return super().get_key_from_id()

    # <editor-fold desc="(to string)">
    def __str__(self) -> str:
        out = super().__str__()  # print abstract class attributes

        if self.__enumvalues_list is not None:
            for sec in self.__enumvalues_list:
                if sec is not None:
                    out += "\n" + str(sec)

        return out
    # </editor-fold>