

class IdFileNameLocation:
    def __init__(self):
        self.__id: str = ""
        self.__file_name_to_write_on: str = ""
        self.__location: str = ""

    @property
    def id(self):
        return self.__id

    @id.setter
    def id(self, i):
        self.__id = i

    @property
    def file_name_to_write_on(self):
        return self.__file_name_to_write_on

    @file_name_to_write_on.setter
    def file_name_to_write_on(self, i):
        self.__file_name_to_write_on = i

    @property
    def location(self):
        return self.__location

    @location.setter
    def location(self, l):
        self.__location = l