from definitions.ParamTagCL import ParamTagCL
from TagTypeEnum import TagTypeEnum


class RefParamTagCL(ParamTagCL):
    __reference_id: str
    __kind_reference: str
    __declaration_name: str
    __definition_value: str

    def __init__(self):
        super().__init__()
        self._tag_type = TagTypeEnum.Ref
        self.__reference_id = None
        self.__kind_reference = None
        self.__declaration_name = None
        self.__definition_value = None

    @property
    def tag_type(self) -> TagTypeEnum:
        return self._tag_type

    @property
    def reference_id(self) -> str:
        return self.__reference_id

    @reference_id.setter
    def reference_id(self, reference_id: str):
        self.__reference_id = reference_id

    @property
    def kind_reference(self) -> str:
        return self.__kind_reference

    @kind_reference.setter
    def kind_reference(self, kind_reference: str):
        self.__kind_reference = kind_reference

    @property
    def declaration_name(self) -> str:
        return self.__declaration_name

    @declaration_name.setter
    def declaration_name(self, declaration_name: str):
        self.__declaration_name = declaration_name

    @property
    def definition_value(self) -> str:
        return self.__definition_value

    @definition_value.setter
    def definition_value(self, definition_value: str):
        self.__definition_value = definition_value

    def __str__(self) -> str:
        out = super().__str__()

        if self.__reference_id is not None:
            out += "\n" + str(self.__reference_id)

        if self.__kind_reference is not None:
            out += "\n" + str(self.__kind_reference)

        if self.__declaration_name is not None:
            out += "\n" + str(self.__declaration_name)

        return out