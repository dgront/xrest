from typing import Optional

from definitions.ParameterDescriptionTagCL import ParameterDescriptionTagCL
from definitions.ParameterNameListTagCL import ParameterNameListTagCL
from definitions.ParameterNameTagCL import ParameterNameTagCL


class ParameterItemTagCL:
    __parameter_name_list_tag: ParameterNameListTagCL
    __parameter_description: ParameterDescriptionTagCL

    def __init__(self):
        self.__parameter_name_list_tag = None
        self.__parameter_description = None

    def get_parameter_name_list(self) -> ParameterNameListTagCL:
        return self.__parameter_name_list_tag

    def set_parameter_name_list(self, parameter_name_list: ParameterNameListTagCL):
        self.__parameter_name_list_tag = parameter_name_list

    def get_parameter_description(self) -> ParameterDescriptionTagCL:
        return self.__parameter_description

    def set_parameter_description(self, parameter_description: ParameterDescriptionTagCL):
        self.__parameter_description = parameter_description

    # <editor-fold desc="(to string)">
    def __str__(self) -> str:
        out = ""

        if self.__parameter_name_list_tag is not None:
            out += str(self.__parameter_name_list_tag)

        if self.__parameter_description is not None:
            out += str(self.__parameter_description)

        return out
    # </editor-fold>

    @classmethod
    def get_test_object(cls):
        parameter_name_tag_cls = ParameterNameTagCL()
        parameter_name_tag_cls.text = "123,"
        parameter_name_list_tag_cl_obj = ParameterNameListTagCL()
        parameter_name_list_tag_cl_obj.set_parameter_name(parameter_name_tag_cls)

        parameter_dec_tag_cl = ParameterDescriptionTagCL()
        parameter_dec_tag_cl.text = "456,"

        pitcl = ParameterItemTagCL()
        pitcl.set_parameter_name_list(parameter_name_list_tag_cl_obj)
        pitcl.set_parameter_description(parameter_dec_tag_cl)

        return pitcl


if __name__ == "__main__":
    ooo = ParameterItemTagCL.get_test_object()
    print(ooo)
