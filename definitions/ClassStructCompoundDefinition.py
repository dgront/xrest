from typing import Optional, List, Any, Dict

from Utils import Utils
from definitions.AbstractCompoundDefinition import AbstractCompoundDefinition

# this class represents a <compountdef> tag in the xml where kind="class" or "struct".
from definitions.AbstractMemberDefinition import AbstractMemberDefinition
from definitions.BaseCompoundRefTagBCL import BaseCompoundRefTagBCL
from definitions.BaseDescription import BaseDescription
from definitions.BriefDescription import BriefDescription
from definitions.CollaborationGraphTagDCL import CollaborationGraphTagDCL
from definitions.DerivedCompoundRefTagBCL import DerivedCompoundRefTagBCL
from definitions.DetailedDescription import DetailedDescription
from definitions.FunctionMemberDefinitionCL import FunctionMemberDefinitionCL
from definitions.IncludesTagBCL import IncludesTagBCL
from definitions.InheritanceGraphTagBCL import InheritanceGraphTagBCL
from definitions.ListOfAllmembers import ListOfAllmembers
from definitions.LocationDefinitionCL import LocationDefinitionCL
from definitions.ParamTagCL import ParamTagCL
from definitions.SectionDefinitionCL import SectionDefinitionCL
from definitions.VariableMemberDefinitionCL import VariableMemberDefinitionCL


class ClassStructCompoundDefinition(AbstractCompoundDefinition):
    # <editor-fold desc="(attributes)">
    #---------------------------------------
    __attr_dictionary: Dict[str, AbstractMemberDefinition]
    __function_dictionary: Dict[str, AbstractMemberDefinition]
    __max_length_of_variable_name: int
    __max_length_of_variable_desc: int
    __max_length_of_function_name: int
    __max_length_of_function_desc: int
    #------------------------------------------
    # </editor-fold>

    # <editor-fold desc="(constructor)">
    def __init__(self):
        super().__init__()
        self.__protection: str = None
        self.__includes: IncludesTagBCL = None
        self.__list_of_all_members: ListOfAllmembers = None
        self.__base_compound_reference: BaseCompoundRefTagBCL = None
        self.__derived_compound_reference: DerivedCompoundRefTagBCL = None
        self.__inheritance_graph: InheritanceGraphTagBCL = None
        self.__collaboration_graph: CollaborationGraphTagDCL = None
    # </editor-fold>

    # <editor-fold desc="(properties)">
    @property
    def protection(self) -> str:
        return self.__protection

    @protection.setter
    def protection(self, protection: str):
        self.__protection = protection
    # </editor-fold>

    # <editor-fold desc="(object accessor)">
    def set_includes(self, includes: IncludesTagBCL):
        self.__includes = includes

    def get_includes(self) -> IncludesTagBCL:
        return self.__includes

    def set_list_of_all_members(self, list_of_all_members: ListOfAllmembers):
        if list_of_all_members is not None:
            self.__list_of_all_members = list_of_all_members

    def get_list_of_all_members(self) -> ListOfAllmembers:
        return self.__list_of_all_members

    def set_base_compound_reference(self, base_compound_reference: BaseCompoundRefTagBCL):
        self.__base_compound_reference = base_compound_reference

    def get_base_compound_reference(self) -> BaseCompoundRefTagBCL:
        return self.__base_compound_reference

    def set_derived_compound_reference(self, derived_compound_reference: DerivedCompoundRefTagBCL):
        self.__derived_compound_reference = derived_compound_reference

    def get_derived_compound_reference(self) -> DerivedCompoundRefTagBCL:
        return self.__derived_compound_reference

    def set_inheritance_graph(self, inheritance_graph: InheritanceGraphTagBCL):
        self.__inheritance_graph = inheritance_graph

    def get_inheritance_graph(self) -> InheritanceGraphTagBCL:
        return self.__inheritance_graph

    def set_collaboration_graph(self, collaboration_graph: CollaborationGraphTagDCL):
        self.__collaboration_graph = collaboration_graph

    def get_collaboration_graph(self) -> CollaborationGraphTagDCL:
        return self.__collaboration_graph
    # </editor-fold>

    def get_namespace_name(self):
        namespace_name_str = super().get_namespace_name()
        return namespace_name_str

    # <editor-fold desc="get_file_name_to_write_on()">
    def get_namespace_name_as_key(self):
        namespace_name_str = self.get_namespace_name()
        namespace_name_as_key_str = "namespace_" + Utils.get_file_name_to_write_on(namespace_name_str)
        return namespace_name_as_key_str

    def get_file_name_to_write_on(self):
        return "class_" + super().get_file_name_to_write_on()

    def get_dir(self) -> str:
        return super().get_dir()
    # </editor-fold>

    # <editor-fold desc="(__str__())">
    def __str__(self):
        out = "\n" + super().__str__()  # print abstract class attributes

        if self.__language is not None:
            out = out + "\n" + str(self.__language)

        if self.__protection is not None:
            out = out + "\n" + str(self.__protection)

        if self.__brief_description is not None:
            out = out + str(self.__brief_description)

        if self.__detailed_description is not None:
            out = out + str(self.__detailed_description)

        if self.__section_def_list is not None:
            for sec in self.__section_def_list:
                if sec is not None:
                    out += "\n" + str(sec)

        if self.__member_def_list is not None:
            for mem in self.__member_def_list:
                if mem is not None:
                    out += "\n" + str(mem)

        if self.__includes is not None:
            out += "\n" + str(self.__includes)

        if self.__location is not None:
            out += "\n" + str(self.__location)

        if self.__list_of_all_members is not None:
            out += "\n" + str(self.__list_of_all_members)

        if self.__base_compound_reference is not None:
            out += "\n" + str(self.__base_compound_reference)

        if self.__derived_compound_reference is not None:
            out += "\n" + str(self.__derived_compound_reference)

        if self.__inheritance_graph is not None:
            out += "\n" + str(self.__inheritance_graph)

        if self.__collaboration_graph is not None:
            out += "\n" + str(self.__collaboration_graph)

        return out
    # </editor-fold>

    # <editor-fold desc="get_functions_rst()">
    def get_functions_rst(self) -> Dict[str, str]:
        return super().get_functions_rst()
    # </editor-fold>

    def get_rst(self):
        return super().get_rst()

    # <editor-fold desc="get_test_object()">
    @classmethod
    def get_test_object(cls):
        csl_obj = ClassStructCompoundDefinition()

        csl_obj.id = "my-id"
        csl_obj.kind = "my-kind"
        csl_obj.compound_name = "my-compound-name"
        csl_obj.language = "my-language"
        csl_obj.protection = "my-protection"

        brief_obj = BaseDescription()
        text_para = ParaTextOnlyTagCL()
        text_para.text = "XXXXX"
        brief_obj.add_simple_para(text_para)

        csl_obj.set_brief_description(brief_obj)

        detail_obj = BaseDescription()
        text_para = ParaTextOnlyTagCL()
        text_para.text = "YYYYY"
        detail_obj.add_simple_para(text_para)

        csl_obj.set_detailed_description(detail_obj)

        return csl_obj

    # </editor-fold>

if __name__ == "__main__":
    cls_obj = ClassStructCompoundDefinition.get_test_object()
    print(cls_obj)
