from definitions.BaseDescription import BaseDescription


class DetailedDescription(BaseDescription):
    def __init__(self):
        super().__init__()

    def __str__(self):
        return "\n" + super().__str__()