# this class represents a <memberdef> node in the xml file.
from typing import Optional

from definitions.BriefDescription import BriefDescription
from definitions.DetailedDescription import DetailedDescription
from definitions.InbodyDescription import InbodyDescription
from definitions.LocationDefinitionCL import LocationDefinitionCL
from abc import ABC, abstractmethod


class AbstractMemberDefinition(ABC):
    # <editor-fold desc="(constructor)">
    def __init__(self) -> object:
        self.__kind: str = None
        self.__id: str = None
        self.__protection: str = None
        self.__static: str = None
        self.__name: str = None
        self.__brief_description: BriefDescription = None
        self.__detailed_description: DetailedDescription = None
        self.__in_body_description: InbodyDescription = None
        self.__location: LocationDefinitionCL = None
        self.__file_name_overload: str = None
    # </editor-fold>

    # <editor-fold desc="(basic type members)">
    @property
    def kind(self) -> str:
        return self.__kind

    @kind.setter
    def kind(self, kind: str):
        self.__kind = kind

    @property
    def id(self) -> str:
        return self.__id

    @id.setter
    def id(self, id: str):
        self.__id = id

    @property
    def protection(self) -> str:
        return self.__protection

    @protection.setter
    def protection(self, protection: str):
        self.__protection = protection

    @property
    def static(self) -> str:
        return self.__static

    @static.setter
    def static(self, static: str):
        self.__static = static

    @property
    def name(self) -> str:
        return self.__name

    @name.setter
    def name(self, name: str):
        self.__name = name

    @property
    def file_name_overload(self) -> str:
        return self.__file_name_overload

    @file_name_overload.setter
    def file_name_overload(self, file_name_overload: str):
        self.__file_name_overload = file_name_overload
    # </editor-fold>

    # <editor-fold desc="(object members)">
    def set_brief_description(self, brief_description: BriefDescription):
        self.__brief_description = brief_description

    def get_brief_description(self) -> BriefDescription:
        return self.__brief_description

    def set_detailed_description(self, detailed_description: DetailedDescription):
        self.__detailed_description = detailed_description

    def get_detailed_description(self) -> DetailedDescription:
        return self.__detailed_description

    def set_in_body_description(self, in_body_description: InbodyDescription):
        self.__in_body_description = in_body_description

    def get_in_body_description(self) -> InbodyDescription:
        return self.__in_body_description

    def set_location(self, location: LocationDefinitionCL):
        self.__location = location

    def get_location(self) -> LocationDefinitionCL:
        return self.__location

    # </editor-fold>

    def get_file_name_to_write_on(self):
        return ""

    # <editor-fold desc="(to string)">
    def __str__(self):
        out = ""

        if self.__kind is not None:
            out = out + "\n" + str(self.__kind)

        if self.__id is not None:
            out = out + "\n" + str(self.__id)

        if self.__protection is not None:
            out = out + "\n" + str(self.__protection)

        if self.__static is not None:
            out = out + "\n" + str(self.__static)

        if self.__name is not None:
            out = out + "\n" + str(self.__name)

        if self.__brief_description is not None:
            out += "\n" + str(self.__brief_description)

        if self.__detailed_description is not None:
            out += "\n" + str(self.__detailed_description)

        if self.__in_body_description is not None:
            out += "\n" + str(self.__in_body_description)

        if self.__location is not None:
            out += "\n" + str(self.__location)

        return out
    # </editor-fold>

    def get_rst(self):
        rst = self.name
        return rst



